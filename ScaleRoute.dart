import 'package:flutter/material.dart';
class ScaleRoute extends PageRouteBuilder {
  final Widget widget;

  ScaleRoute({this.widget})
      : super(
      transitionDuration: Duration(milliseconds: 700),
      pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
        return new ScaleTransition(
          scale: new Tween<double>(
            begin: 0.0,
            end: 1.0,
          ).animate(
            CurvedAnimation(
              parent: animation,
              curve: Interval(
                0.0,
                1.00,
                curve: Curves.easeIn,
              ),
            ),
          ),
          child: widget,
        );
      },
      transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {


        return new ScaleTransition(

          scale: new Tween<double>(
            begin: 0.0,
            end: 1.0,
          ).animate(

            CurvedAnimation(
              parent: animation,
              curve: Interval(
                0.00,
                1.00,
                curve: Curves.easeIn,

              ),
            ),
          ),
          child: child,
        );
      }
  );
}