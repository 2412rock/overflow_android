import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';


class Storage {

  String pem = "";

  Future<String> get localPath async {
    final dir = await getApplicationDocumentsDirectory();
    return dir.path;
  }

  Future<File> get localFile async {
    final path = await localPath;
    return File('$path/joined_game.txt');
  }

  Future<String> readData() async {
    try {
      final file = await localFile;
      String body = await file.readAsString();
      print("Read data: " + body);
      return body;
    } catch (e) {
       print("Error reading data: ");
      //if its first launch
      return "null";
    }
  }

  Future<File> writeData(String data) async {
    final file = await localFile;
   // print("wrote data to file " + data + "   -> data length: " + data.length.toString());
    return file.writeAsString("$data");
  }
}
