
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

import 'dart:convert';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:web_socket_channel/io.dart';
import 'package:flutter/foundation.dart';
import 'User.dart';
import 'package:simple_rsa/simple_rsa.dart';


import '../Game/Coordinate.dart';
import 'ServerKeyGenerator.dart';


class Client{

  final String REGISTERED = "REGISTERED";
  final String WAITING = "WAITING";
  final String REMOVE_MATCH_SUCCESS = "REMOVE_SUCCESS";
  final String REMOVE_MATCH_INGAME = "INGAME";
  final String REMOVE_MATCH_ALREADY_DONE = "ALREADY_REMOVED";
  final String publicKey = 'sm=2P2a=h?-RSuc?';
  final String privateKey = 'WbuQ3*P@H?9bnv+^';
  String encryptionKey;
  WebSocketChannel channel; //"wss://gateway2.xyz/socket";
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  final FirebaseAuth _auth = FirebaseAuth.instance;
  Color buttonColor = Colors.redAccent;
  String idToken;
  String displayName,opponentDisplayName;
  String player_a,player_b,nickname_b;
  String myRank,opponentRank;
  bool updatedOpponentScore;
  final int rankConstant = 16;
  var matchKey;
  String numberOfOnlinePlayers;
  String phoneNr;
  bool logged_in = false;
  bool inGame = false;
  String mail;
  ObserverList<Function> _listeners = new ObserverList<Function>();
  String lobbyServer = "https://overflow-game.com/api";
  String gameRoomServer = "overflow-game.com";
  String myProfilePic,opponentProfilePic;
  List<User> userRanks;
  bool scoreUpdated;
  bool fetchedHighScores;
  DateTime lastPing;

  Client(){
    lastPing = null;
    scoreUpdated = false;
    updatedOpponentScore = false;
    myRank = '42';
    inGame = false;
    fetchedHighScores = false;
    autheticate();
    userRanks = new List();
  }
  
  void pingServer() async{
    if(lastPing == null || DateTime.now().difference(lastPing).inSeconds >= 2) {
      //print("PINGED SERVER");
      String data = jsonEncode({
        "command": "ping",
        "key": this.matchKey,
        "sender": this.mail,
      });
      channel.sink.add(data);
      lastPing = DateTime.now();
    }
  }
  
  int getNumberOfOnlinePlayers(){
  int i;
    try {
      i = int.parse(this.numberOfOnlinePlayers);
  }catch(e){}

    return i == null ? 0 : i;
  }

  Future<DocumentSnapshot> getRankFromDB() async{
    DocumentSnapshot d;
    try {
      await Firestore.instance
          .collection('users')
          .document(this.mail)
          .get()
          .then((DocumentSnapshot ds) async{
        if(ds.data == null){
          //print('Failed to find rank');
          //print('Setting rank');
          await Firestore.instance.collection('users').document(this.mail)
              .setData({ 'rank': 1500, 'pic': myProfilePic, 'nick': getMyNick(), });
          this.myRank = "1500";
        }else{
          //print("Got rank from db");
          //print(ds.data.toString());
          this.myRank = ds.data['rank'].toString();
          //print(myRank);
        }
        d = ds;
      });
    }
    catch(e){
      //print("> FAILED to get rank from DB");
      //print(e.toString());
      d = null;
    }
    return d;
  }
  
  void updateLastGameRank(String lastOpponentRank){
    double myRank = double.parse(this.myRank);
    double opponentRank = double.parse(lastOpponentRank);
    double rankDiff = ( (myRank + opponentRank));
    this.myRank = (myRank + rankConstant * (0 - ( myRank / (rankDiff)))).toString();
    _uploadRankToDB();
  }

  String getRankDiff(String oldRank){
   double diff = double.parse(this.myRank).round() - double.parse(oldRank);
   if (diff > 0){
     return ( '+' + diff.toInt().toString() );
   }else{
     return diff.toInt().toString();
   }
  }

  Future<DocumentSnapshot> updateRank(bool win, {String or}) async{
    DocumentSnapshot result;
    if(this.opponentRank == null){
      this.opponentRank = or;
    }
    if(win && !scoreUpdated){
      double myRank = double.parse(this.myRank);
      double opponentRank = double.parse(this.opponentRank);
      double rankDiff = ( (myRank + opponentRank));
      //print("I won");
      scoreUpdated = true;
      this.myRank = (myRank + rankConstant * (1 - ( myRank / (rankDiff)))).toString();
      result = await _uploadRankToDB();
    }else if(!scoreUpdated){
      double myRank = double.parse(this.myRank);
      double opponentRank = double.parse(this.opponentRank);
      double rankDiff = ( (myRank + opponentRank));
      //print("I lost");
      scoreUpdated = true;
      this.myRank = (myRank + rankConstant * (0 - ( myRank / (rankDiff)))).toString();
      result = await _uploadRankToDB();
    }
    if(result == null) {
      //print("> FAILED to update rank");
    }
    return result;
  }

  void updateOpponentRank(bool win){
      if(win && !updatedOpponentScore){
        updatedOpponentScore = true;
        double myRank = double.parse(this.myRank);
        double opponentRank = double.parse(this.opponentRank);
        double rankDiff = ( (myRank + opponentRank));
        //print("Opponent won");
        this.opponentRank = (opponentRank + rankConstant * (1 - ( opponentRank / (rankDiff)))).toString();

      }else if(!updatedOpponentScore){
        updatedOpponentScore = true;
        double myRank = double.parse(this.myRank);
        double opponentRank = double.parse(this.opponentRank);
        double rankDiff = ( (myRank + opponentRank));
        //print("Opponent lost");
        this.opponentRank = (opponentRank + rankConstant * (0 - ( opponentRank / (rankDiff)))).toString();
      }
  }

  Future<DocumentSnapshot> _uploadRankToDB() async{
    final DocumentReference postRef =  Firestore.instance.collection('users').document(this.mail);
    await Firestore.instance.runTransaction((Transaction tx) async {
      //print('> Found rank to update');
        return await tx.update(postRef, <String, dynamic>{'rank': double.parse(this.myRank)});
    });
    return null;
  }

  String getOpponentNick(){
    if(this.opponentDisplayName != null && opponentDisplayName.contains(' ')){
      String build = "";
      for(int char=0; char<this.opponentDisplayName.length; char++){
        if(char == 13 && opponentDisplayName[char] != ' '){
          build = build + '..';
          return build;
        }
        else if(opponentDisplayName[char] != ' '){
          build += opponentDisplayName[char];
        }else{
          return build;
        }
      }
    }else if(this.opponentDisplayName != null){
      return this.opponentDisplayName;
    }
    else{
      return "";
    }
  }

  String getMyNick(){
    if(this.displayName.contains(' ')){
      String build = "";
      for(int char=0; char<displayName.length; char++){
        if(char == 13 && displayName[char] != ' '){
          build = build + '..';
          return build;
        }
        else if(displayName[char] != ' '){
          build += displayName[char];
        }else{
          return build;
        }
      }
    }else{
      return this.displayName;
    }
  }

  String getOpponentPic(){
    if(this.opponentProfilePic != null){
      return this.opponentProfilePic;
    }
    return "http://www.stleos.uq.edu.au/wp-content/uploads/2016/08/image-placeholder-350x350.png";
  }
  
Future<String> getEmail() async{
    String n;
    await signIn().then((FirebaseUser usr) {
       logged_in = true;
      n = usr.email;
      this.displayName = usr.displayName;
      this.phoneNr = usr.phoneNumber;

    } );
    this.idToken =  ServerKeyGenerator.getKey(n);
    return n;
  }
  
  Future<bool> logout() async{
    bool success;
    await _auth.signOut().then((val) {
      this.logged_in = false;
      success = false;
      _googleSignIn.signOut();
    });
    return success;
  }
  
  Future<String> autheticate() async{
    String mail = await getEmail().whenComplete( ()  {
      //print("I got the email" );
      this.logged_in = true;

    });
    this.mail = mail;
    return mail;
  }
  
  Future<FirebaseUser> signIn() async{
    final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    final FirebaseUser user = await _auth.signInWithCredential(credential);
    myProfilePic =user.photoUrl;
    //print("signed in " + user.displayName);
    return user;
  }

  void signOut() async{
    await _auth.signOut();
  }
  
  Future<bool> register() async {
    try {
      var snd =  lobbyServer + '/hello/' +  (mail) + '/' +  (idToken);
      //print("Sending match req on adress:  " + snd);
      var response = await http.get(snd);
      JsonStatus status = JsonStatus.fromJson(json.decode(response.body));
      if (status.status.toString() == REGISTERED || status.status.toString() == 'ALREADY_REGISTERED' ) {
        return true;
      }
      else {
        return false;
      }
    } catch (e) {
      //print(e);
      return false;
    }
  }

  Future<JsonMatchObject> getOpponent() async{
      try {
        var req = lobbyServer + '/get_opponent/' +  (mail) + '/' +  (idToken);
        var response = await http.get(req);
        //print(req);
        JsonMatchObject data = JsonMatchObject.fromJson(
            json.decode(response.body));
        return data;
      }
      catch(e){
        //print("Error at getOpponent, RETURNING NULL");
      }

      return null;
  }

  Future<String> acquireNumberOfOnlinePlayers() async{
    try {
      var req = lobbyServer + '/hello/' +  'ONLINE_PLAYERS' + '/' +  (idToken);
      var response = await http.get(req);
      this.numberOfOnlinePlayers = response.body.toString();
      return this.numberOfOnlinePlayers;
    }
    catch(e){
      //print("Error at getOpponent, RETURNING NULL");
    }
    return null;
  }


  int whichPlayerAmI(){
    if(player_a == mail){
      return 1;
    }
    else{
      return 2;
    }
  }

  Future<bool> removeMatch() async{
    try{
      //print('REMOVED MATCH');
      //print("------------");
      var req = lobbyServer + '/get_opponent/' +  (mail) + '/' +  (mail) ;
      var response = await http.get(req);
      JsonMatchObject data = JsonMatchObject.fromJson(json.decode(response.body));
      //print("response for remove match");
      //print(response.toString());
      if(data.token.toString().contains(REMOVE_MATCH_SUCCESS) || data.token.toString().contains(REMOVE_MATCH_ALREADY_DONE)){
        return true;
      }
    }catch(e){
      //print("Error at getOpponent() ->>>> RETURN FALSE");
    }
    return false;
  }

  void removeMatchAfterGameEnd() async{
    //print('REMOVED MATCH');
    //print("------------");
    var req = lobbyServer + '/get_opponent/' + 'ENDGAME_DELETE' + '/' +  (this.matchKey.toString()) ;
    await http.get(req);
  }

  Future<JsonMove> fetchOponentMove(var response) async {
    return JsonMove.fromJson(json.decode(response));
  }

  addListener(Function callback){
    _listeners.add(callback);
  }
  removeListener(Function callback){
    _listeners.remove(callback);
  }

  _onReceptionOfMessageFromServer(message){
    _listeners.forEach((Function callback){
      callback(message);
    });
  }

  void resetGameStats(){
    this.player_a = null;
    this.player_b = null;
    this.matchKey = null;
    this.opponentProfilePic = null;
  }

  void initGameSocket(String token, String user_a, String user_b) async{
    channel =  IOWebSocketChannel.connect(new Uri(scheme: "wss", host: gameRoomServer));
    channel.stream.listen(_onReceptionOfMessageFromServer);
    String body = jsonEncode({
            'command':'init',
            "user_a": user_a,
            "user_b": user_b,
            "rank" : myRank,
            "key": token,
            "sender": this.mail,
              "nick" : this.displayName,
            "profile_pic" :this.myProfilePic,

        });
    //print("Sending init command to socket: " + gameRoomServer);
    channel.sink.add( (body));
  }
  
  void askForRematch() async{
    String body = jsonEncode({
      'command':'rematch',
      'key' : this.matchKey,
      'sender': this.mail,
    });

    //print("Sending restart command to socket: ");
    channel.sink.add( (body));
  }
  
  void requestOpponentProfilePic() async{
    String body = jsonEncode({
      'command':'pics',
      'key' : this.matchKey,
      'sender': this.mail,
    });

    //print("Sending pics command to socket: ");
    channel.sink.add( (body));
  }

  void endGameSilent(){
    String body = jsonEncode({
      'command': 'silent_end',
      "key": matchKey,
      "sender": this.mail,

    });
  }

  void sendRematch() async {
    String body = jsonEncode({
      'command': 'rematch',
      "key": matchKey,
      "sender": this.mail,
    });
    channel.sink.add( (body));
  }

  void endGame() async {
    String body = jsonEncode({
      'command': 'end',
      "key": matchKey,
      "sender": this.mail,

    });

    removeMatchAfterGameEnd();
    channel.sink.add( (body));
    resetGameStats();
  }

  bool hasNoListeners(){
    if(_listeners.length == 0){
      return true;
    }else{
      return false;
    }
  }

  void sendMove(Coodrinate lastMove, Coodrinate crtMove) async{
     String body = jsonEncode({
        "command": "move",
        "key": matchKey,
        "user": this.mail,
        "x": crtMove.getX().toString(),
        "y": crtMove.getY().toString(),

      });
     //print('Sent move');
     channel.sink.add( (body));
  }
  
  void askForStart() async{
    String body = jsonEncode({
    "command": "start",
    "key": matchKey,
    "sender": this.mail,
    });
    channel.sink.add( (body) );
  }
}

class JsonMatchObject {
  var token;
  var player_a;
  var player_b;

  JsonMatchObject({this.token, this.player_a, this.player_b});

  factory JsonMatchObject.fromJson(Map<String, dynamic> json) {
      return JsonMatchObject(
      token: json['token'],
      player_a: json['user_a'],
      player_b: json['user_b']

    );
  }
}

class JsonStatus {
  var status;
  JsonStatus({this.status});

  factory JsonStatus.fromJson(Map<String, dynamic> json) {
    return JsonStatus(
        status: json['status'],
      

    );
  }
}


class JsonMove {
  var x;
  var y;
  var user;


  JsonMove({this.x,this.y,this.user});

  factory JsonMove.fromJson(Map<String, dynamic> json) {
    return JsonMove(
      x: json["x"],
      y: json["y"],
      user: json["user"]
    );
  }
}
