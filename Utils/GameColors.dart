import 'package:flutter/material.dart';


class GameColors{

  static final Color red = const Color(0xFFA20021);
  static final Color green = const Color(0xFF08415C);
  static final Color blue = const Color(0xFFF18F01);
  static final Color neutral = const Color(0xFFA2A3BB);

}