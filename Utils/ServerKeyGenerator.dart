class ServerKeyGenerator{

  static final int end = 'z'.codeUnitAt(0);
  static final int start = 'a'.codeUnitAt(0);

  static String _parseUsername(String usr){
    String result = "";
    for(int index=0;index < usr.length; index++){
      if(usr[index] == '@'){
        return result;
      }else{
        result += usr[index];
      }
    }
    return result;
  }

  static String changeChar(String char) {
    int current = char.codeUnitAt(0);
    if (current < end) {
      current ++;
    }else{
      current = start;
    }
    return new String.fromCharCode(current);
  }

  static String getKey(String usr){
    String result = "";
    usr = _parseUsername(usr);
    for(int index=0;index < usr.length; index++){
      result += changeChar(usr[index]);

    }
    return result;
  }
}