import 'package:flutter/material.dart';
import 'package:battery/battery.dart';

class BatteryLvl{
  String batteryLvl;
  VoidCallback changeState;
  Battery battery;

  BatteryLvl(VoidCallback state){
    this.batteryLvl = '100%';
    battery = Battery();
    this.changeState = state;
    _listenForBattery();
  }

  void changeCallback(VoidCallback v){
    this.changeState = v;
  }

  void _listenForBattery(){

    battery.onBatteryStateChanged.listen((BatteryState state) async {
      // Do something with new state

      await battery.batteryLevel.then((percentage) {
        this.batteryLvl = percentage.toString()+'%';
        try {
          changeState();
        }catch(e){

        }});});
  }

  String getBatteryLevel(){
    return this.batteryLvl;
  }

  String getTime(){
    String minutes = DateTime.now().minute.toString();

    if(minutes.length == 1){
      minutes = '0' + minutes;
    }
    return DateTime.now().hour.toString() + ':' + minutes;
  }

}