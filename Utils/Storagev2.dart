import 'dart:async';
import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:flutter/services.dart' show rootBundle;

class StorageCert {

  String pem = "";

  Future<String> get localPath async {
    final dir = await getApplicationDocumentsDirectory();
    return dir.path;
  }

  Future<String> _getPem(String path) async {
    return await rootBundle.loadString('assets/res/certificate.pem');
  }

  Future<File> get localFile async {
    final path = await localPath;
    return File('$path/apikey.pem');
  }

  Future<String> readData() async {
    try {
      final file = await localFile;
      String body = await file.readAsString();
      print("Read data from PEM file: ");
      return body;
    } catch (e) {
       print("Error reading data: ");
      //if its first launch
      return "null";
    }
  }




  Future<File> writeData(String data) async {
    final file = await localFile;
    print("Writing data to local .pem file: " + "   -> data length: " + data.length.toString() + data );
    return file.writeAsString(data);
  }
}
