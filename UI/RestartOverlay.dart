import 'package:flutter/material.dart';
import '../Utils/GameColors.dart';
class RestartOverlay extends StatefulWidget{

  VoidCallback yesCallback,noCallback;
  String txt;

  RestartOverlay(this.txt,this.yesCallback, this.noCallback);

  _RestartOverlayState createState() => new _RestartOverlayState(this.txt,this.yesCallback,this.noCallback);
}

class _RestartOverlayState extends State<RestartOverlay> with SingleTickerProviderStateMixin{


  AnimationController boxController;
  VoidCallback yesCallback,noCallback;
  String txt;

  _RestartOverlayState(this.txt,this.yesCallback,this.noCallback);


  @override
  void initState() {
    // TODO: implement initState
    boxController = AnimationController(vsync: this,duration: Duration(milliseconds: 300));
    boxController.forward();
    super.initState();

  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Widget getMeesage(){
    return Container(
      width: 180,
      height: 100,
      decoration: BoxDecoration(

        border: Border.all(color: Colors.blueGrey,width: 0.3),
        color: GameColors.neutral,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(40),


      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(this.txt,style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),),
          Padding(padding: EdgeInsets.only(top: 10),),
          Container(decoration: BoxDecoration(border: Border(top: BorderSide(color: Colors.black,width: 0.3))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(decoration: BoxDecoration(border: Border(right: BorderSide(color: Colors.black,width: 0.3))),child: MaterialButton(
                      onPressed: () => widget.yesCallback(),
                      child:  Text("Yes", style: TextStyle(fontSize:19, color: GameColors.green,fontWeight: FontWeight.bold),)
                  ),),
                  MaterialButton(
                      onPressed: () => widget.noCallback(),
                      child:  Text("No", style: TextStyle(fontSize:19,color: GameColors.red),)
                  ),
                ],
              ),),
        ],
      ),

    );
  }
  

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
      Center(
        child: AnimatedBuilder(animation: boxController,

        child: getMeesage(),
        builder: (BuildContext context,Widget child) {
              return Transform.scale(scale: boxController.value,child: child,);
    } ));
  }

}