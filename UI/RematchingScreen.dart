import 'package:flutter/material.dart';
import '../Utils/GameColors.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
class RematchingScreen extends StatefulWidget{

  VoidCallback cancelCallback;


  RematchingScreen(this.cancelCallback);

  _RematchingScreenState createState() => new _RematchingScreenState(this.cancelCallback);
}

class _RematchingScreenState extends State<RematchingScreen> with SingleTickerProviderStateMixin{


  AnimationController boxController;
  VoidCallback cancelCallback;
  final String txt = "Waiting opponent";

  _RematchingScreenState(this.cancelCallback);


  @override
  void initState() {
    // TODO: implement initState
    boxController = AnimationController(vsync: this,duration: Duration(milliseconds: 300));
    boxController.forward();
    super.initState();

  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  Widget getMeesage(){
    return Container(
      width: 180,
      height: 100,
      decoration: BoxDecoration(

        border: Border.all(color: Colors.blueGrey,width: 0.3),
        color: GameColors.neutral,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(40),


      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[Text(this.txt,style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.bold),),Padding(padding: EdgeInsets.only(left: 14),), SpinKitCircle(color: GameColors.red, size: 30,)],),
          Padding(padding: EdgeInsets.only(top: 10),),
          Container(decoration: BoxDecoration(border: Border(top: BorderSide(color: Colors.black,width: 0.3))),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(child: MaterialButton(
                  //YES
                  // minWidth: 140,

                  //shape: new RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(40))),
                    onPressed: () => widget.cancelCallback(),
                    child:  Text("Cancel", style: TextStyle(fontSize:19, color: GameColors.green,fontWeight: FontWeight.bold),)

                ),),

              ],
            ),),
        ],
      ),

    );
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
      Center(
          child: AnimatedBuilder(animation: boxController,

              child: getMeesage(),
              builder: (BuildContext context,Widget child) {
                return Transform.scale(scale: boxController.value,child: child,);
              } ));
  }

}