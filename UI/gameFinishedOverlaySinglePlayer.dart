import 'package:flutter/material.dart';
import '../Utils/GameColors.dart';

class GameFinishedOverlay extends StatefulWidget {

  bool iWin,itsATie;
  VoidCallback okCallback,restartCallback;

  GameFinishedOverlay(this.okCallback,this.restartCallback,this.iWin,this.itsATie);

  @override
  State createState() => new _GameFinishedOverlayState(this.okCallback,this.restartCallback,this.iWin, this.itsATie);
}

class _GameFinishedOverlayState extends State<GameFinishedOverlay>
    with SingleTickerProviderStateMixin {

  AnimationController boxController;
  VoidCallback okCallback,restartCallback;
  bool iWin,itsATie;


  _GameFinishedOverlayState(this.okCallback,this.restartCallback,this.iWin,this.itsATie);


  @override
  void initState() {
    boxController = AnimationController(vsync: this,duration: Duration(milliseconds: 400));
    boxController.forward();
    super.initState();

  }

  @override
  void dispose() {
    boxController.dispose();
    super.dispose();
  }

  Widget getMeesage(){
    return Container(
      width: 180,
      height: 100,
      decoration: BoxDecoration(

        border: Border.all(color: Colors.blueGrey,width: 0.3),
        color: GameColors.neutral,
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(40),


      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text(iWin && !itsATie? "Yellow won!" : itsATie ? 'Opponent left!' :"Red won!",style: TextStyle(color: iWin? GameColors.blue :GameColors.red,fontSize: 21),),
          Padding(padding: EdgeInsets.only(top: 10),),

          Container(decoration: BoxDecoration(border: Border(top: BorderSide(color: Colors.black,width: 0.3))),child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              MaterialButton(
                  onPressed: () => widget.okCallback(),
                  child:  Text("Close", style: TextStyle(fontWeight: FontWeight.bold,fontSize:14, color: Colors.black),)

              ),
              itsATie ? Container() : MaterialButton(
                  onPressed: () => widget.restartCallback(),
                  child:  Text("Restart", style: TextStyle(fontWeight: FontWeight.bold,fontSize:14, color: Colors.black),)

              ),

            ],
          ),),
        ],
      ),

    );
  }

  Widget build(BuildContext context) {
    // TODO: implement build
    return
      Center(
          child: AnimatedBuilder(animation: boxController,

              child: getMeesage(),
              builder: (BuildContext context,Widget child) {

                return Transform.scale(scale: boxController.value,child: child,);
              } ));
  }
}
