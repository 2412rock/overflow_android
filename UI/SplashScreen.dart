import 'package:flutter/material.dart';
import 'dart:math' as M;
import 'dart:async';
import 'package:flutter/services.dart';

import '../ScaleRoute.dart';
import 'gamePage.dart';
import '../Game/Game.dart';
import '../Game/Score.dart';
import 'BlankBoard.dart';
import '../Utils/GameColors.dart';
import 'LoginScreen.dart';

class SplashScreen extends StatefulWidget {

  State createState() => new _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;
  bool c1Done,c2Done,c3Done,c4Done;
  Color c1Color,c2Color,c3Color,c4Color;


  _SplashScreenState() {
    c1Done = false;
    c2Done = false;
    c3Done = false;
    c4Done = false;
    c1Color = GameColors.red;
    c2Color = GameColors.green;
    c3Color = GameColors.blue;
    c4Color = Colors.black;
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);


  }

  bool widgetFinished(){
    return c1Done && c2Done && c3Done && c4Done ? true : false;
  }

  @override
  void initState() {
    _iconAnimationController = new AnimationController(
        duration: new Duration(milliseconds: 480), lowerBound: 0,upperBound: 1,vsync: this);
    _iconAnimation = new CurvedAnimation(
        parent: _iconAnimationController, curve: Curves.easeIn);
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward(from: 0);



    super.initState();
  /*
    Timer(Duration(seconds: 4), () {
      // Navigator.of(context).replace(newRoute: new MaterialPageRoute(builder: (BuildContext context) => new FindPage()));
      Navigator.pop(context);
      Navigator.push(context,ScaleRoute(widget: LoginScreen()));

    });

  */
    //  Navigator.of(context).pop();
  }


  Color getColor(int index){
    Color color;
    switch(index){
      case 1:
        color = c1Color;
        //c1Done = true;
        break;
      case 2:
        color = c2Color;
       // c2Done = true;
        break;
      case 3:
        color = c3Color;
       // c3Done = true;
        break;
      default:
        color = c4Color;
       // c4Done = true;
        break;

    }
    return color;
  }

  bool cellShouldInflate(int index){
    if(index == 1){
      if(c1Done){
        return false;
      }else {
        return true;
      }
    }else if(index > 1){
      if(c1Done && index == 2){
        return true;
      }
      else if(c1Done && c2Done && index == 3){
        return true;
      }
      else if(c1Done && c2Done && c3Done && index == 4){
        return true;
      }
      else{
        return false;
      }
    }
  }



  Widget getInflatingCircle(int index){
    Color color = getColor(index);
    if(cellShouldInflate(index)){
      if(_iconAnimationController.isCompleted) {

          // print("Resetting animation for cell ->  " + index.toString());
        _iconAnimationController.reset();
        _iconAnimationController.forward(from: 0);


      }else{
       // print('ongling animation');
      }
    return new Transform.scale(
        scale: _iconAnimation.value,
        child: new Container(
            width: 40.0,
            height: 40.0,
            decoration: BoxDecoration(border: Border.all(color: color == Colors.black ? GameColors.neutral : color),shape: BoxShape.circle,color: color),
        ));
  }else{
      //'new container');
      return getInflatedCircle(-1);
    }

  }

  Widget getInflatedCircle(int index){

    Color color,borderColor;
    if(index > 0 ){
      color = getColor(index);
      borderColor = color == Colors.black ? GameColors.neutral : color;
    }else{
      color = Colors.black;
      borderColor = Colors.black;
    }
    return new Container(
        width: 40.0,
        height: 40.0,
        decoration: BoxDecoration(border: Border.all(color: borderColor),shape: BoxShape.circle,color: color),
    );
  }



  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIOverlays([]);
   // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);

    if(_iconAnimationController.isCompleted){

      print('Done');
      if(!c1Done){
        c1Done = true;

      }else if(!c2Done){
        c2Done = true;
      }
      else if(!c3Done){
        c3Done = true;
      }
      else if(!c4Done){
        c4Done = true;
      }
    }
   // print(_ic)
    return new
    WillPopScope(
        onWillPop: () async => false,
    child:Scaffold(
      backgroundColor: Colors.black,
      body:  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[
                          c1Done ? getInflatedCircle(1) : getInflatingCircle(1),
                          Padding(padding: EdgeInsets.only(left: 10),),
                          c2Done ? getInflatedCircle(2) : getInflatingCircle(2),
                        ],),
                        Padding(padding: EdgeInsets.only(top: 10),),
                        Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[
                          c3Done ? getInflatedCircle(3) : getInflatingCircle(3),
                          Padding(padding: EdgeInsets.only(left: 10),),
                          c4Done ? getInflatedCircle(4) : getInflatingCircle(4),
                        ],)
                      ],
                    )
                  ));

  }
}
