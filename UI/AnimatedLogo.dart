import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../Utils/GameColors.dart';


class AnimatedLogo extends StatefulWidget {

  State createState() => new _AnimatedLogoState();
}

class _AnimatedLogoState extends State<AnimatedLogo>
    with TickerProviderStateMixin {
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;
  bool c1Done,c2Done,c3Done,c4Done;
  Color c1Color,c2Color,c3Color,c4Color;


  _AnimatedLogoState() {
    c1Done = false;
    c2Done = false;
    c3Done = false;
    c4Done = false;
    c1Color = GameColors.red;
    c2Color = GameColors.green;
    c3Color = GameColors.blue;
    c4Color = Colors.black;
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }

  bool widgetFinished(){
    return c1Done && c2Done && c3Done && c4Done ? true : false;
  }

  @override
  void initState() {
    _iconAnimationController = new AnimationController(
        duration: new Duration(milliseconds: 480), lowerBound: 0,upperBound: 1,vsync: this);
    _iconAnimation = new CurvedAnimation(
        parent: _iconAnimationController, curve: Curves.easeIn);
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward(from: 0);

    super.initState();

  }

  Color getColor(int index){
    Color color;
    switch(index){
      case 1:
        color = c1Color;
        break;
      case 2:
        color = c2Color;
        break;
      case 3:
        color = c3Color;
        break;
      default:
        color = c4Color;
        break;
    }
    return color;
  }

  bool cellShouldInflate(int index){
    if(index == 1){
      if(c1Done){
        return false;
      }else {
        return true;
      }
    }else if(index > 1){
      if(c1Done && index == 2){
        return true;
      }
      else if(c1Done && c2Done && index == 3){
        return true;
      }
      else if(c1Done && c2Done && c3Done && index == 4){
        return true;
      }
    }
    return false;
  }

  Widget getInflatingCircle(int index){
    Color color = getColor(index);
    if(cellShouldInflate(index)){
      if(_iconAnimationController.isCompleted) {
        _iconAnimationController.reset();
        _iconAnimationController.forward(from: 0);
      }
      return new Transform.scale(
          scale: _iconAnimation.value,
          child: new Container(
              width: 40.0,
              height: 40.0,
              decoration: BoxDecoration(border: Border.all(color: color == Colors.black ? GameColors.neutral : color),shape: BoxShape.circle,color: color),
          ));
  }else{
      return getInflatedCircle(-1);
    }
  }

  Widget getInflatedCircle(int index){
    Color color,borderColor;
    if(index > 0 ){
      color = getColor(index);
      borderColor = color == Colors.black ? GameColors.neutral : color;
    }else{
      color = Colors.black;
      borderColor = Colors.black;
    }
    return new Container(
        width: 40.0,
        height: 40.0,
        decoration: BoxDecoration(border: Border.all(color: borderColor),shape: BoxShape.circle,color: color),
    );
  }



  Widget build(BuildContext context) {
    if(_iconAnimationController.isCompleted){
      if(!c1Done){
        c1Done = true;
      }else if(!c2Done){
        c2Done = true;
      }
      else if(!c3Done){
        c3Done = true;
      }
      else if(!c4Done){
        c4Done = true;
      }
    }

    return new WillPopScope(onWillPop: () async => false,
        child:Scaffold(
          backgroundColor: Colors.black,
          body:Column(mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Row(mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                c1Done ? getInflatedCircle(1) : getInflatingCircle(1),
                                Padding(padding: EdgeInsets.only(left: 10),),
                                c2Done ? getInflatedCircle(2) : getInflatingCircle(2),
                            ],),
                            Padding(padding: EdgeInsets.only(top: 10),),
                            Row(mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                c3Done ? getInflatedCircle(3) : getInflatingCircle(3),
                                Padding(padding: EdgeInsets.only(left: 10),),
                                c4Done ? getInflatedCircle(4) : getInflatingCircle(4),
                              ],)
                          ],
                        )
                      ));
  }
}
