import 'package:flutter/material.dart';
import '../Game/Game.dart';
import '../Game/Board.dart';
import '../Game/Score.dart';
import 'package:flutter/services.dart';
import 'GameFinishedOverlaySinglePlayer.dart';
import '../Game/Coordinate.dart';
import 'TimerPainter.dart';
import 'dart:math' as math;
import 'dart:async';
import 'RestartOverlay.dart';
import '../ScaleRoute.dart';
import '../Utils/GameColors.dart';
import '../Utils/BatteryLvl.dart';
import '../Utils/SizeConfig.dart';


class GamePageSinglePlayer extends StatefulWidget {
  Game game;
  BatteryLvl batteryLvl;
  //Storage storage;
  GamePageSinglePlayer(Game game,BatteryLvl bl) {
    this.game = game;
    this.batteryLvl = bl;
  }
  _GamePageSinglePlayerState createState() => new _GamePageSinglePlayerState(game,batteryLvl);
}

class _GamePageSinglePlayerState extends State<GamePageSinglePlayer> with TickerProviderStateMixin {


  Color redColor, greenColor, blueColor,neutralColor,player1Color,player2Color,teritoryColor,retryButtonColor;
  final double infoButtonSize = 20.0;
  final double retryButtonSize = 50.0;
  final double donateButtonSize = 20.0;
  final double twoSpacesSize = 28;
  int _timerValue1 = 60;
  int _timerValue2 = 60;
  AnimationController timerController1,timerController2;
  bool gameFinishedOverlayVisible = false;
  bool showRestartScreen;
  bool showGoBackScreen;
  bool exitPopUpVisible = false;
  bool iWIn = false;
  bool itsATie = false;
  bool showPlayer1Timer;
  double textScaleValue = 0;
  int socketRead = 0;
  bool gameOver = false;
  AnimationController restartButtonCOntroller;
  bool player1moves;
  Game game;
  Score score;
  AnimationController _controller1;
  Timer _timer1,_timer2;
  BatteryLvl batteryLvl;


  _GamePageSinglePlayerState(Game game,BatteryLvl b) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    this.game = game;
    redColor = GameColors.red;
    greenColor = GameColors.green;
    blueColor = GameColors.blue;
    neutralColor = GameColors.neutral;
    player1Color = blueColor;
    player2Color = redColor;
    teritoryColor = greenColor;
    retryButtonColor = player1Color;
    player1moves = math.Random().nextInt(2) == 0;
    showRestartScreen = false;
    batteryLvl = b;
    batteryLvl.changeCallback(refresh);
  }

  void refresh(){
    setState(() {});
  }

  void initState() {
    super.initState();
    _controller1 = AnimationController(
      duration: const Duration(milliseconds:400 ),
      vsync: this,
    )
      ..forward();
    timerController1 = AnimationController(
      vsync: this,
      duration: Duration(seconds: _timerValue1),
    );
    timerController2 = AnimationController(
      vsync: this,
      duration: Duration(seconds: _timerValue2),
    );
    timerController1.reverse(from: 1);
    timerController1.stop();
    timerController2.reverse(from: 1);
    timerController2.stop();
    restartButtonCOntroller = new AnimationController(vsync: this,duration: Duration(milliseconds: 1200));
  }



  Widget getCellTxt(int index) {
    Board board = game.getCurrentBoard();
    int ruleNr = board.getCoords(index).getRuleNumber();
    String text = ruleNr == 1 ? " " : ruleNr == 2 ? "II": ruleNr == 3 ? "w" : "r" ;
    Color color = board.getCoords(index).playerIsOnThisPosition(1) || board.getCoords(index).playerIsOnThisPosition(2) ? Colors.black : neutralColor;

    return new Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: <Widget>[
      text == 'II' ?Image.asset("assets/images/jump.png", width: 24, height: 24,color: Colors.white,) :text == 'r'  ? Image.asset("assets/images/dice.png", width: 30, height: 30,color: Colors.white,):text == 'w' ? Image.asset("assets/images/infinite.png", width: 33, height: 33,color: Colors.white,): Text(' ', style: TextStyle(fontFamily: 'Ubuntu',fontSize: twoSpacesSize,color: color),) ,
    ],);
  }

  void setTimer(int pl,bool run){
    const oneSec = const Duration(seconds: 1);
    if(run) {
      if(pl == 1) {
        _timer1 = new Timer.periodic(
            oneSec, (Timer timer) =>
                setState(() {
                  if (_timerValue1 < 1) {
                    gameFinishedOverlayVisible = true;
                    this.iWIn = true;
                    this.itsATie = false;
                    game.score.increasePlayerScore(2);
                    _timer1.cancel();
                    gameOver = true;
                  } else {
                    _timerValue1 = _timerValue1 - 1;
                  }
                }));
      }else{
        _timer2 = new Timer.periodic(
            oneSec,
                (Timer timer) => setState(() {
                  if (_timerValue2 < 1) {
                    _timer2.cancel();
                    gameFinishedOverlayVisible = true;
                    this.iWIn = false;
                    this.itsATie = false;
                    game.getScore().increasePlayerScore(1);
                    gameOver = true;
                  } else {
                    _timerValue2 = _timerValue2 - 1;
                  }
                }));
      }
    }else if(!run){
      if(pl==1) {
        try {
          _timer1.cancel();
        }catch(E){
        }
      }else{
        try {
          _timer2.cancel();
        }catch(E){
        }
      }
    }
  }

  void cancelTimers(){
    _timer1.cancel();
    _timer2.cancel();
    timerController1.stop();
    timerController2.stop();
  }

  void checkAndMovePlayerOnRandom(int player){
    if(game.getCurrentBoard().getCoordForPlayer(player).getRuleNumber() == 4){
      game.getCurrentBoard().getCoordForPlayer(player).movedHere = true;
      List<int> indexList = new List();
      for(int i=0; i<25; i++){
        if(!game.getCurrentBoard().getCoords(i).assertEquals(game.getCurrentBoard().getCoordForPlayer(player) ) && !game.getCurrentBoard().getCoords(i).isBlocked()){
          indexList.add(i);
        }
      }
      if(indexList.length>0) {
        game.getCurrentBoard().movePlayerTo(player, game.getCurrentBoard().getCoords(indexList[1]) );
        game.getCurrentBoard().getCoordForPlayer(player).startTransition();
        if(!game.playerCanStillMove(game.getCurrentBoard().getEnemyForPlayer(player))){
          iWIn = true;
          cancelTimers();
          gameOver = true;
          gameFinishedOverlayVisible = true;
          cancelTimers();
        }
      }else{
        startInvalidMoveAnimation();
      }
    }
  }

  void _toggleMove(var d) async {
    if(!gameOver && _controller1.isCompleted) {
      if (player1moves) {
        if (game.getCurrentBoard().move(d, 1, false)) {
          player1moves = false;
          game.increaseMoves(1);
          setTimer(1, false);
          setTimer(2, true);
          _controller1.reset();
          _controller1.forward();
          timerController1.stop();
          timerController2.stop(canceled: false);
          timerController2.reverse();
          if (!game.playerCanStillMove(2)) {
            iWIn = false;
            game.getScore().increasePlayerScore(1);
            gameFinishedOverlayVisible = true;
            gameOver = true;
            cancelTimers();
          }else if(!game.playerCanStillMove(2)){
            iWIn = true;
            game.getScore().increasePlayerScore(2);
            gameFinishedOverlayVisible = true;
            gameOver = true;
            cancelTimers();
          }else {
            checkAndMovePlayerOnRandom(2);
          }
        }else{
          startInvalidMoveAnimation();
        }
      } else {
        if (game.getCurrentBoard().move(d, 2, false)) {
          player1moves = true;
          game.increaseMoves(2);
          _controller1.reset();
          _controller1.forward();
          setTimer(2, false);
          setTimer(1, true);
          timerController2.stop();
          timerController1.stop(canceled: false);
          timerController1.reverse();
          if (!game.playerCanStillMove(1)) {
            iWIn = true;
            game.getScore().increasePlayerScore(2);
            gameFinishedOverlayVisible = true;
            gameOver = true;
           cancelTimers();
          }else if(!game.playerCanStillMove(2)){
            iWIn = false;
            game.getScore().increasePlayerScore(1);
            gameFinishedOverlayVisible = true;
            gameOver = true;
            cancelTimers();
          }else {
            checkAndMovePlayerOnRandom(1);
          }
        } else{
          startInvalidMoveAnimation();
        }
      }
      try {
        refresh();
      } catch (E) {
        ////////////print("Exception at  _togglemove()");
      }
    }
  }

  void startInvalidMoveAnimation(){
    int player = player1moves ? 1 : 2;
    Coodrinate c = player == 1 ? game.getCurrentBoard().getMyCurrentCoord() : game.getCurrentBoard().getEnemyCoords();
    c.setPlayerOnThisPosition(player, false);
    c.startTransition();
    _controller1.reset();
    _controller1.forward();
    refresh();
    c.setPlayerOnThisPosition(player, true);
    c.startTransition();
    _controller1.reset();
    _controller1.forward();
    refresh();
  }

  void relaunch() {
    Navigator.pop(context);
    Navigator.push(context, ScaleRoute(widget: GamePageSinglePlayer(Game(1,game.getScore() , null), batteryLvl) )   );
  }

  String getMoves() {
    if (player1moves) {
      return game.getMoves(1).toString();
    } else {
      return game.getMoves(2).toString();
    }
  }

  void resetScore() {
    game.score = new Score();
    refresh();
  }

  Widget getAnimatedRestart(){
    restartButtonCOntroller.forward();
    return AnimatedBuilder(animation: restartButtonCOntroller, builder: (BuildContext context, Widget child) {
      return new Transform.rotate(
          angle: restartButtonCOntroller.value * 2 * math.pi,
          child:
          new Icon(
            Icons.refresh,
            size:   46.0,
            color: greenColor,
          )
      );});
  }

  void dispose() {
    _controller1.dispose();
    try {
      _timer1.cancel();
      _timer2.cancel();
      timerController1.dispose();
      timerController2.dispose();
    }
    catch(E){}
    super.dispose();
  }

  Widget getPlayerExpandingCell(int index){
    Coodrinate c = game.getCurrentBoard().getCoords(index);
      return Stack(children: <Widget>[Center(child: Container(width: 65,
        height: 65,
        decoration: BoxDecoration(shape: BoxShape.circle,
            color: Colors.black,
            border: Border.all()),),),
      Center(child: AnimatedBuilder(
          animation: c.playerIsOnThisPosition(1) ? _controller1 : _controller1,
          child: Stack(fit: StackFit.expand,
              children: <Widget>[Center(child: Container(
                decoration: BoxDecoration(
                  boxShadow: getGlow(c),
                    shape: BoxShape.circle, color: c.playerIsOnThisPosition(1) ? player1Color : player2Color),
                width: 65.0,
                height: 65.0,),)
              ]),

          builder: (BuildContext context, Widget child) {
            return Transform.scale(
              scale: c.playerIsOnThisPosition(1) ? getScale(true, 1) : getScale(true, 2),
              child: child,
            );
          }
      )), Center(child: getCellTxt(index),),
      ],);
  }

  List<BoxShadow> getGlow(Coodrinate c){
    bool pl1Here = c.playerIsOnThisPosition(1);
    bool pl2Here = c.playerIsOnThisPosition(2);
    BoxShadow b = new BoxShadow( spreadRadius: pl1Here ? 2 : pl2Here ? 7 : 0, color: player1moves && c.playerIsOnThisPosition(1)? blueColor: !player1moves && c.playerIsOnThisPosition(2)? redColor : Colors.black, blurRadius: 20 );
    List<BoxShadow> list = new List();
    list.add(b);
    return list;
  }

  Widget getCell(int i,int index){
    Coodrinate c = game.getCurrentBoard().getCoords(index);
    if(i == 0 && !(c.playerIsOnThisPosition(1) || c.playerIsOnThisPosition(2) ) && c.tValue()) { //Leave teritory
      c.endTransition();
      return Stack(children: <Widget>[Center(child: Container(padding: EdgeInsets.only(top: 8),child: Image.asset("assets/images/territory.png",width: 23,height: 23,color: Colors.black,), width: 57,height: 57,decoration: BoxDecoration(shape: BoxShape.circle,color: teritoryColor,),)),Center(child:AnimatedBuilder(
          animation:  c.wasOwnedByPlayer(1) ? _controller1 : _controller1,
          child: Stack(fit: StackFit.expand,children: <Widget>[Center(child:Container(decoration: BoxDecoration(shape: BoxShape.circle,color: c.wasOwnedByPlayer(1) ? player1Color : player2Color),width: 65.0, height: 65.0,),),Center(child: getCellTxt(index)),]),
          builder: (BuildContext context, Widget child) {
            return Transform.scale(
              scale: c.wasOwnedByPlayer(1) ? getScale(false, 1) : getScale(false, 2),
              child: child,
            );
          }))]);
    } else if(i == 0 && !(c.playerIsOnThisPosition(1) || c.playerIsOnThisPosition(2) ) && !c.tValue()){ //blank teritory
        return Center(child: Container(padding: EdgeInsets.only(top: 8),child: Image.asset("assets/images/territory.png"),width: 57,height: 57,decoration: BoxDecoration(shape: BoxShape.circle,color: teritoryColor,),));

    } else if(i == 1 && game.getCurrentBoard().getCoords(index).tValue()) { //EXPAND ONTO NEW TERRITORY
          game.getCurrentBoard().getCoords(index).endTransition();
          return getPlayerExpandingCell(index);

    } else if( (c.playerIsOnThisPosition(1) || c.playerIsOnThisPosition(2)) && !c.tValue()){
      return Container(child:Stack(fit: StackFit.expand,children: <Widget>[Center(child:Container(decoration: BoxDecoration(boxShadow: getGlow(c),shape: BoxShape.circle,color: c.wasOwnedByPlayer(1) ? player1Color : player2Color),width: 65.0, height: 65.0,),),Center(child: getCellTxt(index)),]));
    } else{//normal cell
      return GestureDetector( onTap: () {
        //is tappable if I move and cell is reachable
        if(game.getCurrentBoard().cellIsReachableByPlayer(1, index) && player1moves && game.getCurrentBoard().getCoords(index).getRuleNumber() !=4){
          //Coodrinate crt = game.getCurrentBoard().getCoords(index);
         // game.getCurrentBoard().getCoordForPlayer(1).movedFromHere = true;
         // setState(() {


         // game.getCurrentBoard().me = 1;
          Coodrinate crt = game.getCurrentBoard().getCoords(index);
          game.getCurrentBoard().getCoordForPlayer(1).movedFromHere = true;
          game.getCurrentBoard().getCoordForPlayer(1).setPlayerOnThisPosition(1, false);
         // crt.movedFromHere = true;
         // game.getCurrentBoard().moveMeTo(game.getCurrentBoard().getCoords(index).getX(), game.getCurrentBoard().getCoords(index).getY());
          crt.setPlayerOnThisPosition(1, true);
         // game.getCurrentBoard().getMyCurrentCoord().startTransition();
        //  game.getCurrentBoard().getCoordForPlayer(1).startTransition();
          setTimer(1, false);
          setTimer(2, true);
          _controller1.reset();
          _controller1.forward();
          timerController1.stop();
          timerController2.stop(canceled: false);
          timerController2.reverse();
          //  refresh();
          if(!game.playerCanStillMove(2)){
            iWIn = true;
            gameOver = true;
            cancelTimers();
            gameFinishedOverlayVisible = true;
          }else {
            player1moves = false;
          }
         // });
          refresh();
        }else if(game.getCurrentBoard().cellIsReachableByPlayer(2, index) && !player1moves && game.getCurrentBoard().getCoords(index).getRuleNumber() !=4){
        //  Coodrinate crt = game.getCurrentBoard().getCoords(index);
         // game.getCurrentBoard().getCoordForPlayer(2).movedFromHere = true;
        //  setState(() {

            Coodrinate crt = game.getCurrentBoard().getCoords(index);
            game.getCurrentBoard().getCoordForPlayer(2).movedFromHere = true;
            game.getCurrentBoard().getCoordForPlayer(2).setPlayerOnThisPosition(2, false);
            // crt.movedFromHere = true;
            // game.getCurrentBoard().moveMeTo(game.getCurrentBoard().getCoords(index).getX(), game.getCurrentBoard().getCoords(index).getY());
            crt.setPlayerOnThisPosition(2, true);
            _controller1.reset();
            _controller1.forward();
            setTimer(2, false);
            setTimer(1, true);
            timerController2.stop();
            timerController1.stop(canceled: false);
            timerController1.reverse();
          if(!game.playerCanStillMove(1)){
            iWIn = true;
            gameOver = true;
            cancelTimers();
            gameFinishedOverlayVisible = true;
          }else {
            player1moves = true;
          }
         // });
          refresh();
        }
      },
          child:
          player1moves ?
          Stack(fit: StackFit.loose,children: <Widget>[Center(child: Container(width: 65,height: 65,decoration: BoxDecoration(color: Colors.black,border:Border.all(color: getCellBorderColor(index),width: player1moves && game.getCurrentBoard().cellIsReachableByPlayer(1, index)? 1.2 : 0.6 ),shape: BoxShape.circle ),),),Center(child: getCellTxt(index),)],)
          :
          Stack(fit: StackFit.loose,children: <Widget>[Center(child: Container(width: 65,height: 65,decoration: BoxDecoration(color: Colors.black,border:Border.all(color: getCellBorderColor(index),width: !player1moves && game.getCurrentBoard().cellIsReachableByPlayer(2, index)? 1.2 : 0.6 ),shape: BoxShape.circle ),),),Center(child: getCellTxt(index),)],)
      );
    }
  }

  void restartScreen(){

    setState(() {
      showRestartScreen ? showRestartScreen = false : showRestartScreen = true;
    });
  }


  void goBackToMenu(){
    setState(() {
      exitPopUpVisible ? exitPopUpVisible = false : exitPopUpVisible = true;
    });
  }


  Color getCellBorderColor(int index){
    if(player1moves){
      if(game.getCurrentBoard().cellIsReachableByPlayer(1, index)){
        return player1Color;
      }
      else{
        return Colors.white;
      }
    }else{
      if(game.getCurrentBoard().cellIsReachableByPlayer(2, index)){
        return player2Color;
      }
      else{
        return Colors.white;
      }
    }
  }

  void closeRestart(){
    showRestartScreen = false;
    exitPopUpVisible = false;
    refresh();
  }

  void navigateToMainMenu(){
    Navigator.of(context).pop();
  }


  double getScale(bool inflate,int player){
    // ////////////print(_controller.value);
    if(!inflate) {
        return 1 - _controller1.value;
    }
    else{
          return _controller1.value;
        }
  }

  Widget getPlayerBlock(int player) {
    double width = 60;
    double height = 60;
    Text rank = Text(player == 1 ? game.getScore().getPLayerScore(1) : game.getScore().getPLayerScore(2), style: TextStyle(color: neutralColor),);
    AnimationController animation = player == 1 ? timerController1 : timerController2;
    Color color = player1moves && player == 1 ? player1Color : !player1moves && player == 2 ? player2Color : Colors.grey;
    String timerValue = player1moves && player == 1 ? "$_timerValue1" : !player1moves && player == 2 ? "$_timerValue2" : player1moves && player == 2? "$_timerValue2" : "$_timerValue1";
    return new Row(
      children: <Widget>[
        //PLAYER RANK
        player == 1 ?
        Column(
          children: <Widget>[
         //   Text("Score", style: TextStyle(color: neutralColor),),
          //  rank
          ],
        ) : Container(),
        Padding(padding: EdgeInsets.only(left: 20),),
        //PLAYER TIMER
        Stack(
          children: <Widget>[
            Container(

                width: width,
                height: height,
                child: AnimatedBuilder(
                  animation: animation,
                  builder: (BuildContext context,
                      Widget child) {
                    return CustomPaint(
                      painter: TimerPainter(
                        animation: animation,
                        backgroundColor: player == 1 ? player1Color : player2Color,
                        color: Colors.black,
                      ),
                      child: Center(child: Text(timerValue ,textScaleFactor: 1.5,
                        style: TextStyle(color: color,
                            //fontSize: 16,
                            fontFamily: 'Ubuntu'),),),
                    );
                  },
                )
            ),
          ],
        ),
        Padding(padding: EdgeInsets.only(left: 20),),
        player == 2 ?
        Column(
          children: <Widget>[
         //   Text("Score", style: TextStyle(color: neutralColor),),
         //   rank
          ],
        ) : Container(),
      ],
    );
  }

  _onDragUpdate(BuildContext context, DragEndDetails update,bool horizontal) {
    setState(() {
      if(horizontal){
        double horizontal = update.velocity.pixelsPerSecond.dx;
        horizontal < 0 ? _toggleMove("left") : horizontal > 0 ? _toggleMove("right"): () {} ;
      }else{
        double vertical = update.velocity.pixelsPerSecond.dy;
        vertical < 0 ? _toggleMove("up") : vertical > 0 ? _toggleMove("down") : (){};
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 190) / 2;
    final double itemWidth = (size.width) / 2;

    return new WillPopScope(
        onWillPop: () async => false,
        child: new Scaffold(
        backgroundColor: Colors.black,
        body: new Stack(fit: StackFit.loose, children: <Widget>[
        new Column(
          mainAxisAlignment: MainAxisAlignment.center, children: <
          Widget>[
          Padding(padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 1.4),),
          new Expanded(
            child: new Container(
              child: new GestureDetector(
                  onHorizontalDragEnd: (DragEndDetails update) {
                      _onDragUpdate(context, update, true);
                      },
                  onVerticalDragEnd: (DragEndDetails update) {
                      _onDragUpdate(context, update, false);
                  },
                    child: GridView.count(
                      padding: EdgeInsets.only(
                      top: 10.0, bottom: 10.0),
                      childAspectRatio: (itemWidth / itemHeight),
                      mainAxisSpacing: 0.0,
                      crossAxisSpacing: 0.0,
                      crossAxisCount: game.getCurrentBoard().getLength(),
                      physics: ScrollPhysics(parent: NeverScrollableScrollPhysics()),
                      children: new List.generate(game.getCurrentBoard().getSize(), (index) {
                        if(game.getCurrentBoard().getCoords(index).playerIsOnThisPosition(1) ||game.getCurrentBoard().getCoords(index).playerIsOnThisPosition(2) ){
                          return getCell(1, index);
                        }
                        else if(game.getCurrentBoard().getCoords(index).movedFromHere){
                          return getCell(0, index);
                        }
                        else{
                          return getCell(2, index);
                        }
                },
    ))))),
          Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: <Widget>[
            getPlayerBlock(1),
            getPlayerBlock(2),
          ],),
          //space between buttons and stats containers
          new Padding(padding: EdgeInsets.only(top: 45.0)),
          new Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[GestureDetector(onTap: () => goBackToMenu(), child: Icon(Icons.arrow_back,color: redColor,size: retryButtonSize,),),GestureDetector(onTap: () => restartScreen(),child: !gameOver? Icon(Icons.refresh,size: retryButtonSize,color: retryButtonColor,) : getAnimatedRestart()) ],)
          ]
        ),
        gameFinishedOverlayVisible == true
            ? new GameFinishedOverlay( () {
              // User clicked ok
          setState(() {
            gameFinishedOverlayVisible = false;
          });
        },  () => restartScreen(),!this.iWIn, this.itsATie)
            : new Container(),
        showRestartScreen ? RestartOverlay( "Restart?",() => relaunch(), () => closeRestart()) : Container(),
          exitPopUpVisible ? RestartOverlay( "Exit?",() => navigateToMainMenu(), () => closeRestart()) : Container(),

        ])));}

}
