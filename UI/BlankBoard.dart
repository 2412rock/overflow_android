import 'package:flutter/material.dart';
import '../Game/Game.dart';
import 'package:flutter/services.dart';
import '../Utils/GameColors.dart';
import 'dart:math';

class BlankBoard extends StatelessWidget{


  BlankBoard(){
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 190) / 2;
    final double itemWidth = (size.width) / 2;
    Game game = new Game(1,null,null);

    return Scaffold(
        body: Container(
          color: Colors.black,
            child: GridView.count(
                padding: EdgeInsets.only(
                    top: 10.0, bottom: 10.0),
                childAspectRatio: (itemWidth / itemHeight),
                mainAxisSpacing: 0.0,
                crossAxisSpacing: 0.0,
                crossAxisCount: 5,
                physics: ScrollPhysics(parent: NeverScrollableScrollPhysics()),
                children: new List.generate(45, (index) {
                  String text = new Random().nextInt(2) == 1 ? "I" : "II";
                  return Stack(fit: StackFit.loose,children: <Widget>[Center(child: Container(width: 65,height: 65,decoration: BoxDecoration(color: Colors.black,border:Border.all(color: Colors.black),shape: BoxShape.circle ),),),Center(child: Text(text, style: TextStyle(fontSize: 38,color: Colors.white),),)],);
                }))));
    }
  }


