import 'package:flutter/material.dart';
import 'dart:async';
import 'gamePage.dart';
import '../Game/Game.dart';
import 'dart:ui';
import '../Game/Score.dart';
import 'package:flutter/services.dart';
import '../Utils/Client.dart';
import 'LoginScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:io';
import '../ScaleRoute.dart';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/services.dart' show rootBundle;
import 'TutorialPage.dart';
import '../Utils/Storage.dart';
import '../Utils/GameColors.dart';
import 'gamePage_single_player.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'HighScoresPage.dart';

import '../Utils/User.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';


//import 'package:Fire';

class MainMenu extends StatefulWidget {
  Client client;
  ByteData d1,d2;
  MainMenu(this.client,this.d1,this.d2);
  State createState() => new _MainMenuState(client,d1,d2);
}

class _MainMenuState extends State<MainMenu> with SingleTickerProviderStateMixin {


  //Client client;

  //Storage storage;
  bool playButtonPressed;
  AnimationController _controller;
  Animation<double> animation;
  Color red,blue,green,neutral;
  Client client;
  bool showLoadingWidget;
  String board,progress,cancelText;


  _MainMenuState(Client client,d1,d2) {
    progress = "Searching for game";
    cancelText = '(Tap to cancel)';
    showLoadingWidget = false;
    playButtonPressed = false;
    red = GameColors.red;
    green = GameColors.green;
    blue = GameColors.blue;
    neutral = GameColors.neutral;
    this.client = client;
    checkForLeave();
    if(d1 != null && d2 != null) {
      // HttpOverrides.global = new MyHttpOverrides(d1, d2);
    }
    // FirebaseAdMob.instance.initialize(appId: appId);
    //open the local save game file
    //if its not empty check if it should be pushed to firebase
  }
/*
  void checkLocalScoree() async{
    String s = await client.fetchScore();
    if(await storage.readData() != "null"){
      //"=====================Found data locally");
      score = new Score(await storage.readData());
      client.pushScoreToDB(await score.getScore());
     // client.fetchScore();
    }
    else if(s != null){
      //get data from server
      client.fetchScore();
        //"=====================Wrote data locally");
      await storage.writeData(await client.fetchScore());
      client.pushScoreToDB( await storage.readData());
    }
    else{
      client.pushScoreToDB("1500");
      await storage.writeData("1500");
    }
  }
*/
  void checkForLeave() async{
    Storage s = new Storage();
    bool updateLastGameRank;
    String localFileData;
    try {
      localFileData = await s.readData();
      updateLastGameRank = localFileData == 'DONE' ? false : true;
      if(updateLastGameRank){
        print("FOUND PREVIOUS LEAVE ++++++++++++++++++++++++++++++++");
        await s.writeData("DONE");
        client.updateLastGameRank(localFileData);
      }else{
        print("No Leave found +++++++++++++++++++++++++++++++++++++");
      }
    } catch (e){
      updateLastGameRank = false;
    }
  }

  @override
  void initState() {
    // client = new Client();

    //storage = new Storage();
    _controller = AnimationController(vsync: this,duration: Duration());
    animation = CurvedAnimation(parent: _controller, curve: Curves.elasticInOut);
    // animation.addListener(() => this.setState(() {}));
    _controller.forward();
    //"-----init state");
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
   // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.top]);

    //WidgetsBinding.instance.addObserver(this);
    super.initState();
    //  checkLocalScore();
  }

  @override
  void dispose() {
    //"dispose");
    // WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    //"------ ${state.toString()}");
    switch (state) {
      case AppLifecycleState.inactive:
      //"----inactive");
        break;
      case AppLifecycleState.paused:
      //"----paused");
        break;
      case AppLifecycleState.resumed:
      //"-----resumed");
        break;
      case AppLifecycleState.suspending:
      //"-----suspending");
        break;
    }
  }

  void initGame() async {

    // checkLocalScore();

    // Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => new GamePage(new Game(1))));
    // Navigator.popAndPushNamed(context, "/b");

  }


  void handlePlayClick(){
    setState(() {
      playButtonPressed == false ? playButtonPressed = true : playButtonPressed = false;
    });

  }


  Future<JsonBoard> fetchBoard(var response) async {
    return JsonBoard.fromJson(json.decode(response));
  }

  Future<JsonPic> fetchUrl(var response) async{
    return JsonPic.fromJson(json.decode(response));
  }



  void handleBoard(var response) async {
    //"Parssssssssssssing board");
    //response);
    JsonBoard json = await fetchBoard(response);
    List<dynamic> numbers = new List();
    //print(response);
    numbers = json.num;
    String data = "";
    for(int i=0; i< numbers.length; i++){
      if(i == 0) {
        data =  numbers[i].toString() + "P1" ;
      }else if(i == numbers.length - 1){
        data = data + " " + numbers[i].toString() + "P2" ;
      }else{
        data = data + " " + numbers[i].toString();
      }
      //data);
    }
    this.board = data;
    // game = new Game(client.whichPlayerAmI(), Score(), data);


  }




  _onGameData(message) async{
    print("Got message");
    print(message);
    JsonMatchObject data = await client.getOpponent();

    if (data.player_b != client.player_b) {
      client.removeListener(_onGameData);
        return;

    }
    if(message.toString().contains('board')){
      handleBoard(message);
      client.requestOpponentProfilePic();
    }else if(message.toString().contains('opponent_pic')){
      print("Setting opponent profile pic " + message);
      JsonPic pic = await fetchUrl(message);
      client.opponentProfilePic = pic.url.toString();
      client.opponentDisplayName = pic.nick;
      client.opponentRank = pic.rank;
      if(client.opponentDisplayName == null){
        sleep(Duration(milliseconds: 2000));
        client.requestOpponentProfilePic();

      }else{
        Game game = new Game(client.whichPlayerAmI(), Score(), this.board);
        GamePage gamePage = new GamePage(client, game);

        client.removeListener(_onGameData);
        Storage s = new Storage();
        await s.writeData(client.opponentRank);
        print("WROTE OPPONENT SCORE LOCALLY TO PREVENT LEAVE ++++++++++++++++");
        Navigator.pop(context);
        Navigator.push(context,ScaleRoute(widget:gamePage));
      }
    }

  }

  void launchOnlineGamePage() async{
    if(client.logged_in){
      // //"Mail" + await client.getEmail());
      // await client.removeMatch();
      //client.removeMatch();
      client.scoreUpdated = false;
      sleep(Duration(seconds: 1));
      bool success = await client.register();
      success = await client.register();
      if( success){
        print("REGISTERED");
        while(await client.getOpponent() == null){
          print("->>>>>>>>>>>>>>> GETOPPONENT RETURNED NULL");
          if(!playButtonPressed){

            //client.exitLobby();
            bool success = await client.removeMatch();
            if(success){
              return;
            }

          }
          sleep(Duration(milliseconds: 1600));}
        JsonMatchObject data;
        while(true) {
          if (!playButtonPressed) {
            //opponent can be waiting for info on socket when this exits
            //get a response for removeMatch
            bool success = await client.removeMatch();
            if(success){

              return;
            }
            print("No suceess response");
          }
          try {
            data = await client.getOpponent();

            if (data.player_b != client.WAITING) {
              //NEW CHANGE
              client.player_a = data.player_a;
              client.player_b = data.player_b;
              client.matchKey = data.token;
              break;
            } else {

            }
          }catch(e){
            print("Not reigstered");
            print(data);
            progress = 'Server unavailable';
            setState(() {

            });
          }
          sleep(Duration(milliseconds: 1600));
        }
          if (client.player_b != null) {
            //NEW CHANGE
            progress = 'Launching game..';
            cancelText = ' ';
            setState(() {});
            client.addListener(_onGameData);
            client.initGameSocket(data.token, data.player_a, data.player_b);

           // playButtonPressed = false;
            //playButtonPressed = false;
          }


      }else{
        print("Not reigstered");
        progress = 'Server unavailable';
        setState(() {});
      }
    }

  }


  void setLoadingWidget() async{
    if(playButtonPressed && client.player_b != null){
      //cannot cancel if game started
      return;

    }else {
      playButtonPressed = !playButtonPressed ? true : false;
      if (playButtonPressed) {
        // playButtonPressed = false;


        launchOnlineGamePage();
      } else {
        playButtonPressed = false;
        progress = "Searching for game";
      }
    }
    setState(() {

    });
  }

  Widget getPlayOptions(Color txtColor){

    return

    GestureDetector(
      onTap: setLoadingWidget,

      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(progress, style: TextStyle(fontFamily: 'Ubuntu',fontSize: 14, color: GameColors.neutral),),
              Padding(padding: EdgeInsets.only(top: 6),),
              Text(this.cancelText, style: TextStyle(fontFamily: 'Ubuntu',fontSize: 8, color: GameColors.neutral),),
            ],
          ),
          Padding(padding: EdgeInsets.only(left: 6),),
          SpinKitCircle(color: GameColors.red, size: 30,)
        ],
      ),
    );
  }


  Widget getButton(String text,Color borderColor, Color txtColor){

    bool isPlayButton = text == "Online"? true : false;
    bool isTutorialButton = text == "Tutorial" ? true : false;
    bool isHighScoresButton = text == 'Ranks' ? true :false;
    bool isSettingsButton = text == "Log out" ? true : false;
    bool isPracticeButton = text == "Practice" ? true :false;
    bool isDonateButton = text == "Donate" ? true :false;
    VoidCallback buttonCallback = () => isDonateButton ? _launchURL(): isPracticeButton ? launchPracticeGame() :isPlayButton  ? setLoadingWidget() : isTutorialButton ? launchTutorial(): isSettingsButton ? launchSettings(): isHighScoresButton?  launchHighScores():() {};

    return
      Center(
          child:Container(
              width: playButtonPressed && isPlayButton ?  200 : 200,
              height:  playButtonPressed && isPlayButton ?  60 : 50,
              decoration: BoxDecoration(
                border: Border.all(color: borderColor,width: 1),
                shape: BoxShape.rectangle,
                borderRadius: BorderRadius.circular(30),
              ),
              child: Center(child:new RaisedButton(
                  highlightElevation: 2,
                  //animationDuration: Duration(milliseconds: 1000),
                  splashColor:  isPlayButton ? Colors.black : borderColor,
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
                  //textTheme: ButtonTextTheme.primary,

                  color: Colors.black,
                  onPressed: () =>  buttonCallback(),
                  child: new Center(child: playButtonPressed && isPlayButton ? getPlayOptions(txtColor):new Text(text,style: new TextStyle(color: txtColor, fontFamily: 'Ubuntu',fontSize: 30.0, fontWeight: FontWeight.bold)))))));

  }


  _launchURL() async {
    const url = 'https://pornhub.com';

    await launch(url);

  }

  void launchHighScores() async{
    List<User> userRanks = new List();
    Firestore.instance.collection('users').orderBy('rank',descending: true).getDocuments().then((snapshot) {
      snapshot.documents.forEach((d) {
        print("Added user");
        User usr = new User(d.data['pic'] , d.data['nick'] , d.data['rank'].round().toString());
        userRanks.add(usr);
      });
      print("Fetched high scores");
      Navigator.push(context,ScaleRoute(widget:HighScoresPage(userRanks)));
    });



  }


  void launchTutorial(){
    playButtonPressed = false;
    TutorialPage tutorialPage = TutorialPage();
    Navigator.push(context,ScaleRoute(widget:tutorialPage));

  }

  void launchSettings() async{
    client.logout().then((value) {
      client.removeListener(_onGameData);
      showLoadingWidget = false;
      Navigator.pop(context);
      Navigator.push(context,ScaleRoute(widget:LoginScreen()));
    });
  }

  void launchPracticeGame(){
    //  sleep(Duration(milliseconds: 1000));
    playButtonPressed = false;
    Game game = new Game(1,Score(),null);
    GamePageSinglePlayer gamePage = new GamePageSinglePlayer(game);
    // BlankBoard e = BlankBoard();
    Navigator.push(context,ScaleRoute(widget:gamePage));
  }


  @override
  Widget build(BuildContext context) {

    //SystemChrome.setEnabledSystemUIOverlays([]);
    //SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    return  WillPopScope(
        onWillPop: () async => false,
        child:
        MaterialApp(
            home: new Container(
                width: 100,
                height: 200,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new ExactAssetImage('assets/images/menubg.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: new BackdropFilter(
                  filter:  ImageFilter.blur(sigmaX: 4, sigmaY: 4),
                  child: new Container(
                    decoration: new BoxDecoration(color: Colors.white.withOpacity(0.0)),
                    child:  new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        //Container(child: Text("Overflow", style: TextStyle(fontWeight: FontWeight.bold,color: blueGrey,fontFamily: 'Ubuntu',fontSIze: 20))),

                        getButton("Online",neutral,neutral),
                        getButton("Practice", neutral,neutral),
                        //Padding(padding: EdgeInsets.only(bottom:5),),

                        getButton("Tutorial",neutral,neutral),

                        getButton("Ranks", neutral,neutral),
                        getButton("Donate", neutral,neutral),




                      ],
                    ),
                  ),
                )

            )


        ));


    /*new WillPopScope(
        onWillPop: () async => false,bu
        child: Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/images/bulb.jpg"),
                fit: BoxFit.cover,
              ),
            ),
       child: new Scaffold(

            body: new Container(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text("Overflow", style: TextStyle(color: blueGrey,fontFamily: 'Ubuntu',fontSIze: 20)),
                  Padding(padding: EdgeInsets.only(top:10),),
                 getButton("Play",red),
                 getButton("Tutorial",blue),
                 getButton("Settings",green),
                 getButton("Donate", blueGrey),




                ],
              ),
            ))));
            */
  }
}


/*
InterstitialAd myInterstitial = InterstitialAd(
  // Replace the testAdUnitId with an ad unit id from the AdMob dash.
  // https://developers.google.com/admob/android/test-ads
  // https://developers.google.com/admob/ios/test-ads
  adUnitId: "ca-app-pub-2843773508086362/7909316742",
  targetingInfo: targetingInfo,
  listener: (MobileAdEvent event) {
    //"InterstitialAd event is $event");
  },
);
*/
/*
* new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                //Container(child: Text("Overflow", style: TextStyle(fontWeight: FontWeight.bold,color: blueGrey,fontFamily: 'Ubuntu',fontSIze: 20))),

                getButton("Play",neutral,neutral),
            //Padding(padding: EdgeInsets.only(bottom:5),),

                getButton("Tutorial",neutral,neutral),

                getButton("Ranks", neutral,neutral),
                getButton("Donate", neutral,neutral),
                getButton("Settings", neutral,neutral),
                getButton("Socket", neutral, neutral)


              ],
            ),*/