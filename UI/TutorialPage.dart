import 'package:flutter/material.dart';
import 'GamePageOnline.dart';
import '../Game/Game.dart';
import 'package:flutter/services.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'dart:ui';
import 'package:flutter/services.dart';
import '../Utils/GameColors.dart';

class TutorialPage extends StatelessWidget {

  final int numberOfSlides = 9;

   Widget build(BuildContext context) {
     SystemChrome.setEnabledSystemUIOverlays([]);
     SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return new Scaffold(
      backgroundColor:  Colors.black,
      appBar: AppBar(title: Text("How to play", style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),),backgroundColor: Colors.black,),

      body: new InkWell(
        //onTap: () => Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new GamePage(new Game(1)))),
        child: new Swiper(
          itemBuilder: (BuildContext context, int index) {
            String tutorialNumber = (index+1).toString();
            return Container(
              height: 80,
              decoration: BoxDecoration(border: Border.all(color: Colors.white,width: 0.2)),
              child: new Image.asset(
                "assets/images/tutorial/Tutorial " + tutorialNumber +".png",
                fit: BoxFit.fitWidth,
            ));
          },
          itemCount: numberOfSlides,
          viewportFraction: 0.8,
          scale: 0.9,
          control: SwiperControl(iconPrevious: Icons.navigate_before,iconNext: Icons.navigate_next, color: GameColors.blue),
          pagination: SwiperPagination(builder: DotSwiperPaginationBuilder(color: Colors.white,activeColor: GameColors.blue),),
        ),
      ),
    );
  }
}

