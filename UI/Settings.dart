import 'package:flutter/material.dart';
import '../Utils/GameColors.dart';

class Settings extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SettingsState();
  }
}



class _SettingsState extends State<Settings>{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(title: Text("Settings", style: TextStyle(color: GameColors.neutral,fontSize: 20,fontWeight: FontWeight.bold),),backgroundColor: Colors.black,),
      body: Column(
       //mainAxisAlignment: MainAxisAlignment.center,

        children: <Widget>[
          Center(
            child: Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[
              Text("Roman numbers for cells",style: TextStyle(color: GameColors.neutral,fontSize: 20),),
              Switch(inactiveThumbColor: GameColors.red,inactiveTrackColor: GameColors.neutral,activeColor: GameColors.blue,value: false,onChanged: (bool value) {value ? value = false : value = true;setState(() {
              });},)],),
          ),
          Center(
            child: Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[
              Text("Border for blank cells",style: TextStyle(color: GameColors.neutral,fontSize: 20),),
              Switch(inactiveThumbColor: GameColors.red,inactiveTrackColor: GameColors.neutral,activeColor: GameColors.blue,value: false,onChanged: (bool value) {value ? value = false : value = true;setState(() {
              });},)],),
          ),

        ],
      ),
    );
  }
}