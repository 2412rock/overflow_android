import 'package:flutter/material.dart';
import '../Game/Game.dart';
import '../Game/Board.dart';
import '../Game/Score.dart';
import 'package:flutter/services.dart';
import 'dart:convert';
import 'GameFinishedOverlay.dart';
import '../Game/Coordinate.dart';
import 'TimerPainter.dart';
import 'dart:math' as math;
import '../Utils/Client.dart';
import '../Utils/Storage.dart';
import 'dart:async';
import 'RestartOverlay.dart';
import '../ScaleRoute.dart';
import 'NewMainMenu.dart';
import '../Utils/GameColors.dart';
import 'RematchingScreen.dart';
import '../Utils/BatteryLvl.dart';



class GamePage extends StatefulWidget {
  Client client;
  Score score;
  Game game;
  BatteryLvl batteryLvl;
  GamePage(this.client, this.game,this.batteryLvl) ;
  _GamePageState createState() => new _GamePageState(client,game,batteryLvl);
}

class _GamePageState extends State<GamePage> with TickerProviderStateMixin {


  Color redColor, greenColor, blueColor,neutralColor,player1Color,player2Color,teritoryColor,retryButtonColor;
  final double infoButtonSize = 20.0;
  final double retryButtonSize = 50.0;
  final double donateButtonSize = 20.0;
  final double twoSpacesSize = 28;
  int _timerValue1 = 60;
  int _timerValue2 = 60;
  AnimationController timerController1,timerController2;
  bool gameFinishedOverlayVisible = false;
  bool showRestartScreen;
  bool showGoBackScreen;
  bool exitPopUpVisible = false;
  bool iWIn = false;
  bool itsATie = false;
  bool showPlayer1Timer,rematchScreenOpen,rematchRequestScreen;
  String score_a, score_b;
  double textScaleValue = 0;
  int socketRead;
  bool gameOver = false;
  AnimationController restartButtonController;
  bool moveAvailable = false;
  String progress = "";
  JsonMove moveObject;
  bool _enemyLeft;
  Coodrinate lastMove = new Coodrinate(0, 0);
  String first;
  Game game;
  Score score;
  bool movedEnemy;
  AnimationController _controller1;
  Timer _timer1,_timer2,pingTimer;
  Client client;
  BatteryLvl batteryLvl;
  String myRank;


  _GamePageState(Client client,Game game,BatteryLvl b) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    socketRead = 0;
    _enemyLeft = false;
    this.client = client;
    this.client.scoreUpdated = false;
    this.client.updatedOpponentScore = false;
    this.game = game;
    myRank = double.parse(client.myRank).round().toString();
    client.addListener(_onGameData);
    redColor = GameColors.red;
    greenColor = GameColors.green;
    blueColor = GameColors.blue;
    neutralColor = GameColors.neutral;
    movedEnemy = false;
    player1Color = blueColor;
    player2Color = redColor;
    teritoryColor = greenColor;
    retryButtonColor = player1Color;
    showRestartScreen = false;
    rematchScreenOpen  = false;
    rematchRequestScreen = false;
    if (client.whichPlayerAmI() == 1 ) {
      moveAvailable = true ;
      score_a = double.parse(client.myRank).round().toString();
      score_b = double.parse(client.opponentRank).round().toString();
    } else{
      moveAvailable = false;
      score_a = double.parse(client.opponentRank).round().toString();
      score_b = double.parse(client.myRank).round().toString();
    }
    batteryLvl = b;
    b.changeCallback(refresh);
  }

  void initState() {
    super.initState();
    _controller1 = AnimationController(
      duration: const Duration(milliseconds:400 ),
      vsync: this,
    )
      ..forward();
    timerController1 = AnimationController(
      vsync: this,
      duration: Duration(seconds: _timerValue1),
    );
    timerController2 = AnimationController(
      vsync: this,
      duration: Duration(seconds: _timerValue2),
    );
    timerController1.reverse(from: 1);
    timerController1.stop();
    timerController2.reverse(from: 1);
    timerController2.stop();
    pingTimer = Timer(Duration(seconds: 7), () {
      client.pingServer();
    });
    restartButtonController = new AnimationController(vsync: this,duration: Duration(milliseconds: 1200));
  }

  Widget getCellTxt(int index) {

    Board board = game.getCurrentBoard();
    int ruleNr = board.getCoords(index).getRuleNumber();
    String text = ruleNr == 1 ? " " : ruleNr == 2 ? "II": ruleNr == 3 ? "w" : "r" ;
    Color color = board.getCoords(index).playerIsOnThisPosition(1) || board.getCoords(index).playerIsOnThisPosition(2) ? Colors.black : neutralColor;
    return new Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: <Widget>[
      text == 'II' ?Image.asset("assets/images/jump.png", width: 24, height: 24,color: Colors.white,) :text == 'r'  ? Image.asset("assets/images/dice.png", width: 30, height: 30,color: Colors.white,):text == 'w' ? Image.asset("assets/images/infinite.png", width: 33, height: 33,color: Colors.white,): Text(' ', style: TextStyle(fontFamily: 'Ubuntu',fontSize: twoSpacesSize,color: color),) ,
    ],);
  }

  void cancelTimers(){
    try {
      _timer1.cancel();
    }
    catch(e){
      //print('Failed to cancel timer 1');
    }
    try{
      timerController1.stop();
    }
    catch(e){
      //print('Failed to cancel timer 1');
    }
    try{
      _timer2.cancel();
    }catch(e){
      //print('Failed to cancel timer 2');
    }
    try{
      timerController2.stop();
    }
    catch(e){
      //print('Failed to cancel timer 1');
    }
  }

  void moveMe() {
    client.sendMove(game.getCurrentBoard().getMyCurrentCoord(), game.getCurrentBoard().getMyCurrentCoord());
    moveAvailable = false;
    setTimer(game.getCurrentBoard().me, false);
    setTimer(game.getCurrentBoard().getEnemyPlayer(), true);
    _controller1.reset();
    _controller1.forward();
    refresh();
    if (game.getCurrentBoard().me == 1) {
      timerController1.stop();
      timerController2.stop(canceled: false);
      timerController2.reverse();
    } else {
      timerController2.stop();
      timerController1.stop(canceled: false);
      timerController1.reverse();
    }
    movedEnemy = false;
  }

  void _toggleMove(var d) async {
    if(game.playerCanStillMove(game.getCurrentBoard().me)) {
      if (!gameOver && _controller1.isCompleted) { //4 for randomness!!
        if (moveAvailable && game.getCurrentBoard().getMyCurrentCoord().getRuleNumber() != 3 && game.getCurrentBoard().getMyCurrentCoord().getRuleNumber() != 4) {
          if (game.getCurrentBoard().move(d, game.getCurrentBoard().me, false)) {
            moveMe();
            if(!game.playerCanStillMove(game.getCurrentBoard().getEnemyPlayer())){
              iWIn = true;
              gameOver = true;
              cancelTimers();
              gameFinishedOverlayVisible = true;
            }
          }
          else {
            startInvalidMoveAnimation();
          }
        }
      }
      refresh();
    }
  }

  void startInvalidMoveAnimation(){
    int player = moveAvailable ? game.getCurrentBoard().me : game.getCurrentBoard().getEnemyPlayer();
    Coodrinate c = moveAvailable ? game.getCurrentBoard().getMyCurrentCoord() : game.getCurrentBoard().getEnemyCoords();

    c.setPlayerOnThisPosition(player, false);
    c.startTransition();
    _controller1.reset();
    _controller1.forward();
    refresh();

    c.setPlayerOnThisPosition(player, true);
    c.startTransition();
    _controller1.reset();
    _controller1.forward();
    refresh();

  }

  void refresh() {
    setState(() {});
  }

  Widget getAnimatedRestart(){
    restartButtonController.forward();
    return AnimatedBuilder(animation: restartButtonController, builder: (BuildContext context, Widget child) {
      return new Transform.rotate(
          angle: restartButtonController.value * 2 * math.pi,
          child:
          new Icon(
            Icons.refresh,
            size:   46.0,
            color: greenColor,
          )
      );});
  }

  void dispose() {
    gameFinishedOverlayVisible = false;
    exitPopUpVisible = false;
    try {
      _controller1.dispose();
      _timer1.cancel();
      _timer2.cancel();
      timerController1.dispose();
      timerController2.dispose();
    }
    catch(E){
    }
    super.dispose();
  }


  Widget getPlayerExpandingCell(int index){
    Coodrinate c = game.getCurrentBoard().getCoords(index);

    return Stack(children: <Widget>[Center(child: Container(width: 65,
      height: 65,
      decoration: BoxDecoration(shape: BoxShape.circle,
          color: Colors.black,
          border: Border.all()),),),
    Center(child: AnimatedBuilder(

        animation: _controller1,

        child: Stack(fit: StackFit.expand,
            children: <Widget>[Center(child: Container(
              decoration: BoxDecoration(
                  boxShadow: getGlow(c),
                  shape: BoxShape.circle, color: c.playerIsOnThisPosition(1) ? player1Color : player2Color),
              width: 65.0,
              height: 65.0,),)
            ]),
        builder: (BuildContext context, Widget child) {
          return Transform.scale(
            scale: c.playerIsOnThisPosition(1) ? getScale(true, 1) : getScale(true, 2),
            child: child,
          );
        }
    )), Center(child: getCellTxt(index),),
    ],);
  }

  List<BoxShadow> getGlow(Coodrinate c){
    bool pl1Here = c.playerIsOnThisPosition(1);
    bool pl2Here = c.playerIsOnThisPosition(2);
    BoxShadow b = new BoxShadow( spreadRadius: pl1Here ? 2 : pl2Here ? 7 : 0, color: pl1Here && game.getCurrentBoard().me == 1 && moveAvailable? blueColor: pl1Here && game.getCurrentBoard().getEnemyPlayer() == 1 &&!moveAvailable ? blueColor: pl2Here && moveAvailable && game.getCurrentBoard().me == 2? redColor: pl2Here && !moveAvailable && game.getCurrentBoard().getEnemyPlayer() == 2? redColor : Colors.black, blurRadius: 20 );
    List<BoxShadow> list = new List();
    list.add(b);
    return list;
  }

  Widget getCell(int i,int index){
    Coodrinate c = game.getCurrentBoard().getCoords(index);
    if(i == 0 && !(c.playerIsOnThisPosition(1) || c.playerIsOnThisPosition(2) ) && c.tValue()) { //Leave teritory
      c.endTransition();
      return Stack(children: <Widget>[Center(child: Container(padding: EdgeInsets.only(top: 8),child: Image.asset("assets/images/territory.png",width: 23,height: 23,color: Colors.black,), width: 57,height: 57,decoration: BoxDecoration(shape: BoxShape.circle,color: teritoryColor,),)),Center(child:AnimatedBuilder(
          animation:   _controller1,
          child: Stack(fit: StackFit.expand,children: <Widget>[Center(child:Container(decoration: BoxDecoration(shape: BoxShape.circle,color: c.wasOwnedByPlayer(1) ? player1Color : player2Color),width: 65.0, height: 65.0,),),Center(child: getCellTxt(index)),]),
          builder: (BuildContext context, Widget child) {
            return Transform.scale(
              scale: c.wasOwnedByPlayer(1) ? getScale(false, 1) : getScale(false, 2),
              child: child,
            );
          }))]);
    } else if(i == 0 && !(c.playerIsOnThisPosition(1) || c.playerIsOnThisPosition(2) ) && !c.tValue()){ //blank teritory
      return Center(child: Container(padding: EdgeInsets.only(top: 8),child: Image.asset("assets/images/territory.png"),width: 57,height: 57,decoration: BoxDecoration(shape: BoxShape.circle,color: teritoryColor,),));

    } else if(i == 1 && game.getCurrentBoard().getCoords(index).tValue()) { //EXPAND ONTO NEW TERRITORY
      game.getCurrentBoard().getCoords(index).endTransition();
      return getPlayerExpandingCell(index);

    } else if( (c.playerIsOnThisPosition(1) || c.playerIsOnThisPosition(2)) && !c.tValue()){
      return Container(child:Stack(fit: StackFit.expand,children: <Widget>[Center(child:Container(decoration: BoxDecoration(boxShadow: getGlow(c),shape: BoxShape.circle,color: c.wasOwnedByPlayer(1) ? player1Color : player2Color),width: 65.0, height: 65.0,),),Center(child: getCellTxt(index)),]));
    } else{//normal cell
      return GestureDetector( onTap: () {
        //is tappable if I move and cell is reachable
        if(game.getCurrentBoard().cellIsReachableByPlayer(client.whichPlayerAmI(), index) && moveAvailable && game.getCurrentBoard().getMyCurrentCoord().getRuleNumber() !=4){
          Coodrinate crt = game.getCurrentBoard().getMyCurrentCoord();
          crt.movedFromHere = true;
          game.getCurrentBoard().moveMeTo(game.getCurrentBoard().getCoords(index).getX(), game.getCurrentBoard().getCoords(index).getY());
          game.getCurrentBoard().getMyCurrentCoord().startTransition();
        //  refresh();
          if(!game.playerCanStillMove(game.getCurrentBoard().getEnemyPlayer())){
            iWIn = true;
            gameOver = true;
            cancelTimers();
            gameFinishedOverlayVisible = true;
          }
          moveMe();
        }
      },
          child: Stack(fit: StackFit.loose,children: <Widget>[Center(child: Container(width: 65,height: 65,decoration: BoxDecoration(color: Colors.black,border:Border.all(color: getCellBorderColor(index),width: moveAvailable && game.getCurrentBoard().cellIsReachableByPlayer(game.getCurrentBoard().me, index)? 1 : 0.6 ),shape: BoxShape.circle ),),),Center(child: getCellTxt(index),)],)
      );
    }
  }

  void restartScreen(){

    setState(() {
      showRestartScreen ? showRestartScreen = false : showRestartScreen = true;
    });
  }

  void goBackToMenu(){
    setState(() {

      exitPopUpVisible ? exitPopUpVisible = false : exitPopUpVisible = true;
    });
  }

  Color getCellBorderColor(int index){
    if(moveAvailable){

      if(game.getCurrentBoard().cellIsReachableByPlayer(1, index) && game.getCurrentBoard().me == 1){
        return player1Color;
      }
      else if(game.getCurrentBoard().cellIsReachableByPlayer(2, index) && game.getCurrentBoard().me == 2){
        return player2Color;
      }
    }
    return Colors.white;
  }

  void closeRestart(){
    showRestartScreen = false;
    exitPopUpVisible = false;
    refresh();
  }

  void navigateToMainMenu() async{
    cancelTimers();
    if(!gameOver){
        Navigator.of(context).pop();
        Navigator.push(context, ScaleRoute(widget: MainMenu(client, null, null)));
    }else {
      client.endGame();
      client.player_b = null;
      client.removeListener(_onGameRestartData);
      client.removeListener(_onGameData);
      client.player_a = null;
      Navigator.of(context).pop();
      Navigator.push(context, ScaleRoute(widget: MainMenu(client, null, null)));
    }
  }

  double getScale(bool inflate,int player){
    if(!inflate) {
      return 1 - _controller1.value;
    }
    else{
      return _controller1.value;
    }
  }

  Widget getImage( int pl){
    return Container(
      width: 34,
      height: 34,
      decoration: BoxDecoration(shape: BoxShape.circle,image: new DecorationImage(
          fit: BoxFit.fill,
          image: new NetworkImage(
              pl == 1 && game.getCurrentBoard().me == 1 ? client.myProfilePic :
              pl == 1 && game.getCurrentBoard().getEnemyPlayer() == 1 && client.opponentProfilePic != null ? client.opponentProfilePic   :
              pl == 2 && game.getCurrentBoard().me == 2 ? client.myProfilePic :
              pl == 2 && game.getCurrentBoard().getEnemyPlayer() == 2 && client.opponentProfilePic != null ? client.opponentProfilePic   :
              "")
      )),
    );
  }

  Widget _getTimer(int player, Animation animation, int me, int enemy){
    double width = 45;
    double height = 45;
    String timerValue =  player == 1 ? "$_timerValue1" : "$_timerValue2";
    Animation animation =  player == 1 ? timerController1 : timerController2;
    Color color = moveAvailable && player == 1 && me == 1 ? player1Color : moveAvailable && player == 2 && me == 2? player2Color: !moveAvailable && player == 1 && enemy == 1 ? player1Color : !moveAvailable && player == 2 && enemy == 2 ? player2Color : teritoryColor;
    return Container(
        width: width,
        height: height,
        child: AnimatedBuilder(
          animation: animation,
          builder: (BuildContext context,
              Widget child) {
            return CustomPaint(
              painter: TimerPainter(
                animation: animation,
                backgroundColor: player == 1 ? player1Color : player2Color,
                color: Colors.black,
              ),
              child: Center(child: Text(timerValue ,
                textScaleFactor: 1.3,
                style: TextStyle(color: player == 1 && client.whichPlayerAmI() == 1 && moveAvailable ? player1Color:
                  player == 2 && client.whichPlayerAmI() == 2 && moveAvailable? player2Color : player == 2 && ! moveAvailable && client.whichPlayerAmI() == 1? player2Color:
                  player == 1 && !moveAvailable && client.whichPlayerAmI() == 2? player1Color : Colors.grey,
                    fontFamily: 'Ubuntu',
                    ),),),
            );
          },
        )
    );
  }

  Widget getPlayerBlock(int player) {
    int me =   game.getCurrentBoard().me ;
    int enemy = game.getCurrentBoard().getEnemyPlayer();
    double nameFontSize = 26;
    double rankFontSize = 14;
    AnimationController animation = player == 1 ? timerController1 : timerController2;
    String nick_a = client.whichPlayerAmI() == 2 ? client.getMyNick() : client.getOpponentNick();
    String img_a = client.whichPlayerAmI() == 2 ? client.myProfilePic : client.getOpponentPic();
    String nick_b =  client.whichPlayerAmI() == 1  ? client.getMyNick() : client.getOpponentNick();
    String img_b = client.whichPlayerAmI() == 1  ? client.myProfilePic : client.getOpponentPic();

    if(player == 1) {
      return Container(
        decoration: BoxDecoration(border: Border(bottom: BorderSide(color: Colors.white,width: 0.6))),
        padding: EdgeInsets.only(top: 12,bottom: 12,left: 10),
        child:  Row(
          // mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            //Name and rank
            Container(width: 33,height: 33,decoration: BoxDecoration(shape: BoxShape.circle,image: DecorationImage(fit: BoxFit.fill,image: NetworkImage(img_b)),)),
            Padding(padding: EdgeInsets.only(right: 20),),
            Row(
              children: <Widget>[
                //nick name
                Text(nick_b,style: TextStyle(fontFamily: 'Ubuntu',fontSize: nameFontSize, color: GameColors.blue,fontWeight: FontWeight.bold),),
                Padding(padding: EdgeInsets.only(left: 6),),
                //score
                client.whichPlayerAmI() == 1 ? getMyRank():Text("(" + score_a + ")", style: TextStyle(fontFamily: 'Ubuntu',fontSize: rankFontSize, color: GameColors.neutral,fontWeight: FontWeight.bold),),
              ],
            ),
            Padding(padding: EdgeInsets.only(left: 40),),
            _getTimer(player, animation, me, enemy),
          ],
        ),
      );
    }else{
      return Container(
        decoration: BoxDecoration(border: Border(top: BorderSide(color: GameColors.neutral,width: 0.9))),
        padding: EdgeInsets.only(top: 14,right: 10),
        child: Row(

          children: <Widget>[
            // Padding(padding: EdgeInsets.only(left: 20),),
            _getTimer(player, animation, me, enemy),
            Padding(padding: EdgeInsets.only(left: 40),),
            Row(
              children: <Widget>[
                //nick name
                Text(nick_a,style: TextStyle(fontFamily: 'Ubuntu',fontSize: nameFontSize, color: GameColors.red, fontWeight: FontWeight.bold),),
                Padding(padding: EdgeInsets.only(left: 6),),
                client.whichPlayerAmI() == 2 ? getMyRank() : Text("(" + score_b+ ")", style: TextStyle(fontFamily: 'Ubuntu',fontSize: rankFontSize, fontWeight: FontWeight.bold,color: GameColors.neutral),),
              ],
            ),
            Padding(padding: EdgeInsets.only(right: 20),),
            Container(width: 33,height: 33,decoration: BoxDecoration(shape: BoxShape.circle,image: DecorationImage(fit: BoxFit.fill,image: NetworkImage(img_a)),)),
            // Padding(padding: EdgeInsets.only(left: 20),),
          ],
        ),
      );
    }

  }

  _onDragUpdate(BuildContext context, DragEndDetails update,bool horizontal) {
    setState(() {
      if(horizontal){
        double horizontal = update.velocity.pixelsPerSecond.dx;
        horizontal < 0 ? _toggleMove("left") : horizontal > 0 ? _toggleMove("right"): () {} ;
      }else{
        double vertical = update.velocity.pixelsPerSecond.dy;
        vertical < 0 ? _toggleMove("up") : vertical > 0 ? _toggleMove("down") : (){};
      }

    });
  }

  void handleOponentMove(var move) async {
    JsonMove js = await client.fetchOponentMove(move);
    int x = int.parse(js.x);
    int y = int.parse(js.y);
    try {
      if (!(game.getCurrentBoard().getEnemyCoords().getX() == x &&
          game.getCurrentBoard().getEnemyCoords().getY() == y)) {
        _controller1.reset();
        _controller1.forward();
        Coodrinate crt = game.getCurrentBoard().getEnemyCoords();
        crt.movedFromHere = true;
        game.getCurrentBoard().moveEnemyTo(int.parse(js.x), int.parse(js.y));
        game.getCurrentBoard().getEnemyCoords().startTransition();
        refresh();
        if(game.getCurrentBoard().me == 1) {
          timerController2.stop();
          timerController1.stop(canceled: false);
          timerController1.reverse();
        }else{
          timerController1.stop();
          timerController2.stop(canceled: false);
          timerController2.reverse();
        }
        this.lastMove = new Coodrinate(int.parse(js.x), int.parse(js.y));
        moveObject = js;
        moveAvailable = true;
        movedEnemy = true;
        setTimer(game.getCurrentBoard().me, true);
        setTimer(game.getCurrentBoard().getEnemyPlayer(), false);
        if (!game.playerCanStillMove(game.getCurrentBoard().me)) {
          iWIn = false;
          cancelTimers();
            if (!gameOver) {
              gameFinishedOverlayVisible = true;
            }
            gameOver = true;
        }else if(game.getCurrentBoard().getMyCurrentCoord().getRuleNumber() == 4){
          game.getCurrentBoard().getMyCurrentCoord().movedHere = true;
          List<int> indexList = new List();
          for(int i=0; i<25; i++){
            if(!game.getCurrentBoard().getCoords(i).assertEquals(game.getCurrentBoard().getMyCurrentCoord() ) && !game.getCurrentBoard().getCoords(i).isBlocked()){
              indexList.add(i);
            }
          }
          if(indexList.length>0) {
            game.getCurrentBoard().moveMeTo(
                game.getCurrentBoard().getCoords(indexList[1]).getX(),
                game.getCurrentBoard().getCoords(indexList[1]).getY());
            game.getCurrentBoard().getMyCurrentCoord().startTransition();
            moveMe();
            if(!game.playerCanStillMove(game.getCurrentBoard().getEnemyPlayer())){
              iWIn = true;
              cancelTimers();
              gameOver = true;
              gameFinishedOverlayVisible = true;
            }
          }else{
            startInvalidMoveAnimation();
          }
        }
        refresh();
      }
    }
    catch(e){}
  }

  void enemyLeft() async{
    iWIn = true;
    if(!gameOver) {
      gameFinishedOverlayVisible = true;
    }

    gameOver = true;
    itsATie = true;
     await client.updateRank(true).then((val) {
      client.endGame();
      refresh();
      setTimer(1, false);
      setTimer(2, false);
      client.removeListener(_onGameData);
      client.removeListener(_onGameRestartData);
      refresh();
      cancelTimers();
      });
  }

  void okCallback() async{
    client.endGame();
    cancelTimers();
    client.removeListener(_onGameData);
    client.removeListener(_onGameRestartData);
    Navigator.of(context).pop();
    Navigator.push(context,ScaleRoute(widget:MainMenu(client,null,null)));
  }

  void exit()async{
    client.endGame();
    if(!gameOver){
      await client.updateRank(false).then((v) {navigateToMainMenu();});
    }
  }


  void askForRematch() async{
    rematchScreenOpen = true;
     await client.updateRank(iWIn).then((val) {
      client.updateOpponentRank(!iWIn);
      client.askForRematch();
      refresh();
    });
  }

  Future<JsonBoard> fetchBoard(var response) async {
    return JsonBoard.fromJson(json.decode(response));
  }

  Future<JsonPic> fetchUrl(var response) async{
    return JsonPic.fromJson(json.decode(response));
  }

  Future<String> handleBoard(var response) async {
    JsonBoard json = await fetchBoard(response);
    List<dynamic> numbers = new List();
    numbers = json.num;
    String data = "";
    for(int i=0; i< numbers.length; i++){
      if(i == 0) {
        data =  numbers[i].toString() + "P1" ;
      }else if(i == numbers.length - 1){
        data = data + " " + numbers[i].toString() + "P2" ;
      }else{
        data = data + " " + numbers[i].toString();
      }
    }
    return data;
  }


  _onGameRestartData(message) async{
    //print("Got message");
    //print(message);
    if(message.toString().contains('board')){
      String board = await handleBoard(message);
      client.requestOpponentProfilePic();
      client.removeListener(_onGameData);
      client.removeListener(_onGameRestartData);
      Game game = new Game(client.whichPlayerAmI(), Score(), board);
      GamePage gamePage = new GamePage(client, game,batteryLvl);
      cancelTimers();
        Navigator.pop(context);
        Navigator.push(context,ScaleRoute(widget:gamePage));
      }
  }


  _onGameData(message) async{
    String data = message.toString();
    //print(message);
    String opponent = client.whichPlayerAmI() == 1 ? client.player_b : client.player_a;
    if( data.contains('status_opponent')){
      if(data.contains('left')){
        enemyLeft();
      }else if(data.contains('TIME_OUT')){
        if(moveAvailable){
          iWIn = false;
        }else{
          iWIn = true;
        }
        if(!gameOver) {
          gameFinishedOverlayVisible = true;
        }
        client.updateRank(iWIn).then((val) {
          gameOver = true;
          setState(() {});
        });
      }
    }
    if( data.contains('rematch')){
      client.initGameSocket(client.matchKey, client.player_a, client.player_b);
      client.addListener(_onGameRestartData);
    }
    if(data.contains("x") && data.contains(opponent) && !movedEnemy){
      handleOponentMove(data);
    }if(data.contains("OPPONENT_LEFT") ){
      if(!iWIn && _enemyLeft == false) {
        _enemyLeft = true;
        enemyLeft();
      }
    }if(data.contains("restart")){
      showRestartScreen = true;
    }
  }

  Widget getMyRank(){
    double rankFontSize = 14;
    if(gameFinishedOverlayVisible){
      return Row(
        children: <Widget>[
          Text( '(' + this.myRank,style: TextStyle(fontFamily: 'Ubuntu',fontSize: rankFontSize, color: GameColors.neutral,fontWeight: FontWeight.bold),),
          Text(client.getRankDiff(this.myRank) , style: TextStyle(fontFamily: 'Ubuntu',fontSize: rankFontSize-3, color: client.getRankDiff(this.myRank).contains('-') ? Colors.red : Colors.green),),
          Text(')', style: TextStyle(fontFamily: 'Ubuntu',fontSize: rankFontSize, color: GameColors.neutral,fontWeight: FontWeight.bold))
        ],
      );
    }else{
      return Text( '(' + double.parse(client.myRank).round().toString()+ ')',style: TextStyle(fontFamily: 'Ubuntu',fontSize: rankFontSize, color: GameColors.neutral,fontWeight: FontWeight.bold),);
    }
  }

  void updateLocalFile() async{
    Storage s = new Storage();
    //print("GAME FINISHED NORMALLY ++++++++++++++++");
    await s.writeData("DONE");
  }

  void rankU() async{
    await client.updateRank(iWIn);
  }

  @override
  Widget build(BuildContext context) {
    if(!pingTimer.isActive) {
      client.pingServer();
      pingTimer = Timer(Duration(seconds: 7), (){});
    }
    if(gameFinishedOverlayVisible){
      if(gameOver){
        rankU();
        gameOver = false;
      }
    }
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 190) / 2;
    final double itemWidth = (size.width) / 2;
    return new WillPopScope(
        onWillPop: () async => false,
        child: new Scaffold(
            backgroundColor: Colors.black,
            body: new Stack(fit: StackFit.loose, children: <Widget>[
              new Column(
                  mainAxisAlignment: MainAxisAlignment.center, children: <
                  Widget>[

                Row(mainAxisAlignment: MainAxisAlignment.start,children: <Widget>[
                  getPlayerBlock(1),

                  Padding(padding: EdgeInsets.only(right: 10),),
                ],),
                new Expanded(
                    child: new Container(
                        child: new GestureDetector(
                            onHorizontalDragEnd: (DragEndDetails update) {
                              _onDragUpdate(context, update, true);
                            },
                            onVerticalDragEnd: (DragEndDetails update) {
                              _onDragUpdate(context, update, false);
                            },
                            child: GridView.count(
                                padding: EdgeInsets.only(
                                    top: 10.0, bottom: 10.0),
                                childAspectRatio: (itemWidth / itemHeight),
                                mainAxisSpacing: 0.0,
                                crossAxisSpacing: 0.0,
                                crossAxisCount: game.getCurrentBoard().getLength(),
                                physics: ScrollPhysics(parent: NeverScrollableScrollPhysics()),
                                children: new List.generate(game.getCurrentBoard().getSize(), (index) {
                                  if(game.getCurrentBoard().getCoords(index).playerIsOnThisPosition(game.getCurrentBoard().me) ||game.getCurrentBoard().getCoords(index).playerIsOnThisPosition(game.getCurrentBoard().getEnemyPlayer()) ){
                                    return getCell(1, index);
                                  }
                                  else if(game.getCurrentBoard().getCoords(index).movedFromHere){
                                    return getCell(0, index);
                                  }
                                  else{
                                    return getCell(2, index);
                                  }
                                },
                                ))))),
                Padding(padding: EdgeInsets.only(top: 10),),
                Row(mainAxisAlignment: MainAxisAlignment.end,children: <Widget>[
                  Padding(padding: EdgeInsets.only(top: 10),),
                  getPlayerBlock(2),
                ],),
                //space between buttons and stats containers
                new Padding(padding: EdgeInsets.only(top: 34.0)),
                new Row(mainAxisAlignment: MainAxisAlignment.center,children: <Widget>[GestureDetector(onTap: () => goBackToMenu(), child: Icon(Icons.arrow_back,color: redColor,size: retryButtonSize,),), ],)
              ]
              ),
              gameFinishedOverlayVisible == true
                  ? new GameFinishedOverlay(() =>  updateLocalFile(),() => okCallback(), () => askForRematch(),!this.iWIn, this.itsATie)
                  : new Container(),

              rematchScreenOpen ? RematchingScreen( () => okCallback()) : Container(),
              exitPopUpVisible ? RestartOverlay( "Exit?",() => exit(), () => closeRestart()) : Container(),

            ])));}


  void setTimer(int pl,bool run){
    const oneSec = const Duration(seconds: 1);
    if(run) {
      if(pl == 1) {
        try {
          _timer1 = new Timer.periodic(
              oneSec,
                  (Timer timer) =>
              this.mounted ? setState(() {
                if (_timerValue1 < 1) {
                  if(game.getCurrentBoard().me == 2){
                    if(!gameOver) {
                      gameFinishedOverlayVisible = true;
                    }
                    this.iWIn = true;
                    this.itsATie = false;
                    game.score.increasePlayerScore(game.getCurrentBoard().getEnemyPlayer());
                    cancelTimers();
                    gameOver = true;
                  }else {
                    if(!gameOver) {
                      gameFinishedOverlayVisible = true;
                    }
                    this.iWIn = false;
                    this.itsATie = false;
                    game.score.increasePlayerScore(game.getCurrentBoard().getEnemyPlayer());
                    cancelTimers();
                    gameOver = true;
                  }if(game.getCurrentBoard().me == 2){
                    if(!gameOver) {
                      gameFinishedOverlayVisible = true;
                    }
                    this.iWIn = true;
                    this.itsATie = false;
                    game.score.increasePlayerScore(game.getCurrentBoard().getEnemyPlayer());
                    cancelTimers();
                    gameOver = true;
                  }else {
                    if(!gameOver) {
                      gameFinishedOverlayVisible = true;
                    }
                    this.iWIn = false;
                    this.itsATie = false;
                    game.score.increasePlayerScore(game.getCurrentBoard().getEnemyPlayer());
                    cancelTimers();
                    gameOver = true;
                  }
                } else {
                  _timerValue1 = _timerValue1 - 1;
                }
              }) : () => {});
        }catch (e){}
      }else{
        try {
          _timer2 = new Timer.periodic(
              oneSec,
                  (Timer timer) =>
              this.mounted ? setState(() {
                if (_timerValue2 < 1) {
                  if(game.getCurrentBoard().me == 1){
                    if(!gameOver) {
                      gameFinishedOverlayVisible = true;
                    }
                    this.iWIn = true;
                    this.itsATie = false;
                    game.score.increasePlayerScore(game.getCurrentBoard().getEnemyPlayer());
                    try {
                      _timer1.cancel();
                    }catch(e){}
                    gameOver = true;
                  }else {
                    if(!gameOver) {
                      gameFinishedOverlayVisible = true;
                    }
                    this.iWIn = false;
                    this.itsATie = false;
                    game.score.increasePlayerScore(game.getCurrentBoard().getEnemyPlayer());
                    try {
                      _timer1.cancel();
                    }catch(e){}
                    gameOver = true;
                  }
                  game.getScore().increasePlayerScore(game.getCurrentBoard().me);
                  gameOver = true;
                } else {
                  _timerValue2 = _timerValue2 - 1;
                }
              }) : () => {} );
        }catch(e){}
      }
    }else if(!run){
      if(pl==1) {
        try {
          _timer1.cancel();
        }catch(E){}
      }else{
        try {
          _timer2.cancel();
        }catch(E){}
      }
    }
  }

}

class JsonBoard {
  var num;
  var first;

  JsonBoard({this.num, this.first});

  factory JsonBoard.fromJson(Map<String, dynamic> json) {
    return JsonBoard(
      num: json["board"],
      //first: json["first"],
    );
  }
}

class JsonPic {
  var url;
  var nick;
  var rank;

  JsonPic({this.url,this.nick,this.rank});

  factory JsonPic.fromJson(Map<String, dynamic> json) {
    return JsonPic(
        url: json["opponent_pic"],
        nick: json['nick'],
        rank: json['rank']
    );
  }
}

