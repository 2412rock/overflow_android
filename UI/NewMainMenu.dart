import 'package:flutter/material.dart';
import 'dart:async';
import 'GamePageOnline.dart';
import '../Game/Game.dart';
import 'dart:ui';
import '../Game/Score.dart';
import 'package:flutter/services.dart';
import '../Utils/Client.dart';
import 'LoginScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:io';
import '../ScaleRoute.dart';
import '../Utils/BatteryLvl.dart';
import 'dart:typed_data';
import '../Utils/SizeConfig.dart';
import 'TutorialPage.dart';
import '../Utils/Storage.dart';
import '../Utils/GameColors.dart';
import 'package:battery/battery.dart';
import 'GamePageSinglePlayer.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'HighScoresPage.dart';

import '../Utils/User.dart';
import 'dart:convert';
import 'package:url_launcher/url_launcher.dart';


class MainMenu extends StatefulWidget {
  Client client;
  ByteData d1,d2;
  MainMenu(this.client,this.d1,this.d2);
  State createState() => new _MainMenuState(client);
}

class _MainMenuState extends State<MainMenu> with SingleTickerProviderStateMixin {
  bool playButtonPressed;
  AnimationController _controller;
  Animation<double> animation;
  Color red,blue,green,neutral;
  Client client;
  bool showLoadingWidget,initialClockRun;
  String board,progress,cancelText;
  SizeConfig conf;
  var battery = Battery();

  BatteryLvl batteryLvl;

  _MainMenuState(Client client) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    progress = "Searching for game";
    cancelText = '(Tap to cancel)';
    showLoadingWidget = false;
    playButtonPressed = false;
    initialClockRun = true;
    red = GameColors.red;
    green = GameColors.green;
    blue = GameColors.blue;
    neutral = GameColors.neutral;
    batteryLvl = new BatteryLvl(refresh);
    this.client = client;
    conf = new SizeConfig();
    checkForLeave();
  }

  void refresh(){
    setState(() {

    });
  }

  void checkForLeave() async{
    Storage s = new Storage();
    bool updateLastGameRank;
    String localFileData;
    try {
      localFileData = await s.readData();
      updateLastGameRank = localFileData == 'DONE' ? false : true;
      if(updateLastGameRank){
        ////print("FOUND PREVIOUS LEAVE ++++++++++++++++++++++++++++++++");
        await s.writeData("DONE");
        client.updateLastGameRank(localFileData);
      }else{
        ////print("No Leave found +++++++++++++++++++++++++++++++++++++");
      }
    } catch (e){
      updateLastGameRank = false;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Size getCellSize(){
    return Size(SizeConfig.blockSizeHorizontal * 25, SizeConfig.blockSizeHorizontal * 25);
  }

  void _changeBlankCellColor(){

  }

  Widget n(String upperText, String downText){
    return Column(
      children: <Widget>[
        Text(upperText,textScaleFactor: 0.80, style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
        Padding(padding: EdgeInsets.only(top: 6),),
        Text(downText,textScaleFactor: 0.70, style: TextStyle(color: Colors.white),)
      ],
    );
  }

  Widget getOnlinePressedButtonAnimation(){
    return Column(
      children: <Widget>[
        SpinKitCircle(color: Colors.white, size: 35),
        Padding(padding: EdgeInsets.only(top: 10),),
        Text("Online:" + client.getNumberOfOnlinePlayers().toString(),textScaleFactor: 0.6,style: TextStyle(color: Colors.white,),),
      ],
    );
  }

  Widget getCell(VoidCallback onClick, String text,Color color){
   return Container(decoration: BoxDecoration(border: Border.all(color: color == Colors.black ? Colors.white: Colors.black,),shape: BoxShape.circle),child: new RawMaterialButton(
     constraints: BoxConstraints(minWidth: SizeConfig.blockSizeHorizontal * 26,minHeight: SizeConfig.blockSizeVertical * 13.8),
      onPressed: ()  => onClick(),
       child: text == 'Play' && !playButtonPressed ? Text(text,textScaleFactor: 1.2, style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontFamily: 'Ubuntu'),) :
       text == 'Play' && playButtonPressed ? getOnlinePressedButtonAnimation() :Text(text,textScaleFactor: 1.2, style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontFamily: 'Ubuntu'),),
      shape: new CircleBorder(),
      elevation: 2.0,
      fillColor: color,
      padding: const EdgeInsets.all(15.0),
     splashColor: Colors.white,
   ));
  }

  Color getCellColor(int index){
    Color c;
    switch(index){
      case 1:
        c = red;
        break;
      case 2:
        c = green;
        break;
      case 3:
        c = blue;
        break;
      case 4:
        c = red;
        break;
      case 5:
        c = green;
        break;
      default:
        c = Colors.black;
    }
    return c;
  }

  String getCellText(int index){
    String t;
    switch(index){
      case 1:

        t = 'Play';
        break;
      case 2:
        t = 'Practice';
        break;
      case 3:
        t = 'Tutorial';
        break;
      case 4:
        t = 'Ranking';
        break;
      case 5:
        t = '<3';
        break;
      default:
        t = ' ';
    }
    return t;
  }

  VoidCallback getCellFunction(int index){
    VoidCallback f;
    switch(index){
      case 1:
        f = launchOnlineGamePage;
        break;
      case 2:
        f = launchPracticeGame;
        break;
      case 3:
        f = launchTutorial;
        break;
      case 4:
        f = launchHighScores;
        break;
      case 5:
        f = _launchURL;
        break;
      default:
        f = (){};
    }
    return f;
  }

  void refreshOnlinePlayers() async{
    client.acquireNumberOfOnlinePlayers().then((val) {
      setState(() {

      });
    });
  }
  List<Widget> getCellsForColumn(int columnNumber){
    //
    List<Widget> list = new List();
    for(int i=1;i<=5;i++) {
      if (i % 2 == 0) {
        if (columnNumber == 1) {
          if(i == 2){
            list.add(getCell(refreshOnlinePlayers, " ", Colors.black));
          }else {
            list.add(getCell(_changeBlankCellColor, " ", Colors.black));
          }//add online players
          list.add(Padding(padding: EdgeInsets.only(bottom: 30)));
        } else {
          list.add(getCell(getCellFunction(i), getCellText(i), getCellColor(i)));
          list.add(Padding(padding: EdgeInsets.only(bottom: 30)));
        }
      } else {
        if (columnNumber == 1) {
          list.add(getCell(getCellFunction(i), getCellText(i), getCellColor(i)));
          if(i < 5) {
            list.add(Padding(padding: EdgeInsets.only(bottom: 30)));
          }
        } else {
          if(i == 1){
            list.add(getCell(_changeBlankCellColor, '', Colors.black));
          }else if(i==3){
            list.add(getCell(_changeBlankCellColor, "", Colors.black));
          }else{
            list.add(getCell(_changeBlankCellColor, " ", Colors.black));
          }
          if(i < 5) {
            list.add(Padding(padding: EdgeInsets.only(bottom: 30)));
          }
        }
      }
    }
    return list;
  }


  void launchHighScores() async{
    List<User> userRanks = new List();
    Firestore.instance.collection('users').orderBy('rank',descending: true).getDocuments().then((snapshot) {
      snapshot.documents.forEach((d) {
        User usr = new User(d.data['pic'] , d.data['nick'] , d.data['rank'].round().toString());
        userRanks.add(usr);
      });
      Navigator.push(context,ScaleRoute(widget:HighScoresPage(userRanks,client.myProfilePic,client.myRank)));
    });
  }

  void setLoadingAnimation(){
    if(playButtonPressed){
      playButtonPressed = false;

    }else{
      playButtonPressed = true;

    }
    setState(() {

    });
  }

  void launchOnlineGamePage() async{
    setLoadingAnimation();
    if(client.logged_in){
      client.scoreUpdated = false;
      sleep(Duration(seconds: 1));
      bool success = await client.register();
      success = await client.register();
      if( success){
        ////print("REGISTERED");
        while(await client.getOpponent() == null){
          ////print("->>>>>>>>>>>>>>> GETOPPONENT RETURNED NULL");
          if(!playButtonPressed){
            bool success = await client.removeMatch();
            if(success){
              return;
            }
          }
          sleep(Duration(milliseconds: 1600));}
        JsonMatchObject data;
        while(true) {
          if (!playButtonPressed) {
            bool success = await client.removeMatch();
            if(success){
              return;
            }
            ////print("No suceess response");
          }
          try {
            data = await client.getOpponent();
            if (data.player_b != client.WAITING) {
              //NEW CHANGE
              client.player_a = data.player_a;
              client.player_b = data.player_b;
              client.matchKey = data.token;
              break;
            }
          }catch(e){
            ////print(data);
            progress = 'Server unavailable';
            setState(() {

            });
          }
          sleep(Duration(milliseconds: 1600));
        }
        if (client.player_b != null) {
          progress = 'Launching game..';
          cancelText = ' ';
          setState(() {});
          client.addListener(_onGameData);
          client.initGameSocket(data.token, data.player_a, data.player_b);
        }
      }else{
        ////print("Not reigstered");
        progress = 'Server unavailable';
        setState(() {});
      }
    }
  }

  _onGameData(message) async{
    ////print("Got message");
    ////print(message);
    JsonMatchObject data = await client.getOpponent();
    if (data.player_b != client.player_b) {
      client.removeListener(_onGameData);
      return;
    }
    if(message.toString().contains('board')){
      handleBoard(message);
      client.requestOpponentProfilePic();
    }else if(message.toString().contains('opponent_pic')){
      ////print("Setting opponent profile pic " + message);
      JsonPic pic = await fetchUrl(message);
      client.opponentProfilePic = pic.url.toString();
      client.opponentDisplayName = pic.nick;
      client.opponentRank = pic.rank;
      if(client.opponentDisplayName == null){
        sleep(Duration(milliseconds: 2000));
        client.requestOpponentProfilePic();

      }else{
        Game game = new Game(client.whichPlayerAmI(), Score(), this.board);
        GamePage gamePage = new GamePage(client, game,batteryLvl);

        client.removeListener(_onGameData);
        Storage s = new Storage();
        await s.writeData(client.opponentRank);
        ////print("WROTE OPPONENT SCORE LOCALLY TO PREVENT LEAVE ++++++++++++++++");
        Navigator.pop(context);
        Navigator.push(context,ScaleRoute(widget:gamePage));
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    //"------ ${state.toString()}");
    switch (state) {
      case AppLifecycleState.inactive:
      //"----inactive");
        break;
      case AppLifecycleState.paused:
      //"----paused");
        break;
      case AppLifecycleState.resumed:
      //"-----resumed");
        break;
      case AppLifecycleState.suspending:
      //"-----suspending");
        break;
    }
  }

  Future<JsonBoard> fetchBoard(var response) async {
    return JsonBoard.fromJson(json.decode(response));
  }

  Future<JsonPic> fetchUrl(var response) async{
    return JsonPic.fromJson(json.decode(response));
  }

  void handleBoard(var response) async {

    JsonBoard json = await fetchBoard(response);
    List<dynamic> numbers = new List();
    //////print(response);
    numbers = json.num;
    String data = "";
    for(int i=0; i< numbers.length; i++){
      if(i == 0) {
        data =  numbers[i].toString() + "P1" ;
      }else if(i == numbers.length - 1){
        data = data + " " + numbers[i].toString() + "P2" ;
      }else{
        data = data + " " + numbers[i].toString();
      }
    }
    this.board = data;
  }

  _launchURL() async {
    const url = 'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=4TFMLRNYBVVJ4&source=url';
    await launch(url);
  }

  void launchTutorial(){
    playButtonPressed = false;
    TutorialPage tutorialPage = TutorialPage();
    Navigator.push(context,ScaleRoute(widget:tutorialPage));

  }

  void launchSettings() async{
    client.logout().then((value) {
      client.removeListener(_onGameData);
      showLoadingWidget = false;
      Navigator.pop(context);
      Navigator.push(context,ScaleRoute(widget:LoginScreen()));
    });
  }

  void launchPracticeGame(){
    playButtonPressed = false;
    Game game = new Game(1,Score(),null);
    GamePageSinglePlayer gamePage = new GamePageSinglePlayer(game,batteryLvl);
    Navigator.push(context,ScaleRoute(widget:gamePage));
  }



  @override
  Widget build(BuildContext context) {

    conf.init(context);
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(

        backgroundColor: Colors.black,
        body: Column(
        children: <Widget> [
          Padding(padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 0.8),),
          Container(
          padding: EdgeInsets.only(top: SizeConfig.blockSizeVertical * 4),
          child: Column(
            children: <Widget>[
              new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Column(children: getCellsForColumn(1),),
             // Padding(padding: EdgeInsets.only(left: 40),),
                  Column(children: getCellsForColumn(2),)
          ],
        ),
            //  Text('Overflow',textScaleFactor: 1.2,style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontFamily: 'Ubuntu'),),
          ]
      )
      )])),
    );
  }
}

