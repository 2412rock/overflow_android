import 'package:flutter/material.dart';
import '../Utils/Client.dart';
import '../Utils/GameColors.dart';
import '../ScaleRoute.dart';
import 'NewMainMenu.dart';
import 'AnimatedLogo.dart';
import 'package:flutter/services.dart';

class LoginScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State<LoginScreen>{

  Client client;
  Color spinnerColor,txtColor;
  bool done,init;

  _LoginScreenState(){
      done = false;
      init = true;
      client = new Client();
      spinnerColor = GameColors.red;
      txtColor = GameColors.neutral;
    }


    void waitForAuth(BuildContext context) async{
    if(!done) {
      await client.autheticate().then((onValue) async {
        await client.getRankFromDB().then((ds) async{
          if (ds != null) {
            print("got ranks");
          } else {
            print("Failed to get/set rank");
          }
              setState(() {
                spinnerColor = Colors.black;
                txtColor = Colors.black;
                done = true;
          });
          await client.acquireNumberOfOnlinePlayers().then((onValue) async{
             Navigator.push(context, ScaleRoute(widget: MainMenu(client,null,null)));
          });
        });
      });
    }
    }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    if(init) {
      waitForAuth(context);
      init = false;
    }
    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 160,
              height: 160,
              child: AnimatedLogo(),
            ),
            Padding(padding: EdgeInsets.only(top: 12),),
            Text('Loggin in..', style: TextStyle(color: txtColor, fontSize: 20, fontFamily: 'Ubuntu'),),
          ],),
        ),
      ),
    );
  }
}