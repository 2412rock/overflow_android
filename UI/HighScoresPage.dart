import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../Utils/User.dart';
import '../Utils/GameColors.dart';
import '../Utils/SizeConfig.dart';

class HighScoresPage extends StatefulWidget{
  List<User> users;
  String myPic,myRank;
  HighScoresPage(this.users,this.myPic,this.myRank);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HighScoresPageState(users,myPic,myRank);
  }
}

class _HighScoresPageState extends State<HighScoresPage>{

  List<User> users;
  String myPic,myRank;
  _HighScoresPageState(List<User> usrs,String pic,String rank) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    this.users = usrs;
    this.myPic = pic;
    this.myRank = rank;
  }


  Widget getMyPic(){
    String pic = myPic;
    return Container(
        width: 30,
        height: 30,
        //padding: EdgeInsets.only(left: 10),
        decoration: BoxDecoration(shape: BoxShape.circle,image: new DecorationImage(
            fit: BoxFit.fill,
            image: new NetworkImage(pic) )));
  }

  Widget getMyScore(){
    String rank = double.parse(myRank).round().toString();
    return Text(rank, style: TextStyle(color: GameColors.neutral,fontSize: 19,fontFamily: 'Ubuntu'),);
  }

  Widget getUserPic(int index){
    String pic = users[index].getPic();
    return Container(
        width: 30,
        height: 30,

        decoration: BoxDecoration(shape: BoxShape.circle,image: new DecorationImage(
        fit: BoxFit.fill,
        image: new NetworkImage(pic) )));
  }

  Widget getUserNick(int index){
    String nick = users[index].getNick();
    return Text(nick, style: TextStyle(color: GameColors.neutral,fontSize: 19,fontFamily: 'Ubuntu'),);

  }

  Widget getUserRank(int index){
    String rank = double.parse(users[index].getRank()).round().toString();
    return Text(rank, style: TextStyle(color: GameColors.neutral,fontSize: 19,fontFamily: 'Ubuntu'),);
  }

  Widget getIndex(int index){
    index = index + 1;
    return Text(index.toString(), style: TextStyle(color: GameColors.neutral,fontSize: 19,fontFamily: 'Ubuntu'),);
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    var size = MediaQuery.of(context).size;
    final double itemHeight = 35;
    final double itemWidth = (size.width) / 2;
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(title: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,children: <Widget>[
        Text("High Scores", style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold,fontFamily: 'Ubuntu'),),
        Padding(padding: EdgeInsets.only(left: SizeConfig.blockSizeHorizontal * 2),),
        getMyPic(),
        getMyScore()
      ],),backgroundColor: Colors.black,),
      body: GridView.count(
        mainAxisSpacing: 0.0,
        crossAxisSpacing: 0.0,
        childAspectRatio: (itemWidth / itemHeight),
        crossAxisCount: 1,

      shrinkWrap: true,
      children: List.generate(users.length, (index) {
        return  Container(

          width: 20,
          decoration: BoxDecoration( color: (index + 1 ) % 2 == 0 ? Colors.black: const Color(0xFF010c1e)  ,border: Border(bottom: BorderSide(color: Colors.black))),
          child: Row(
            //mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
                  Padding(padding: EdgeInsets.only(left: 30),),
                  getIndex(index),
                  Padding(padding: EdgeInsets.only(left: 10),),
                  getUserPic(index),
                  Padding(padding: EdgeInsets.only(left: 13),),
                  getUserNick(index),

            Row(
              children: <Widget>[
                  Padding(padding: EdgeInsets.only(left: 40),),
                getUserRank(index),
              ],
            )
            ],
          ),
        );
      }),)
    );
  }

}