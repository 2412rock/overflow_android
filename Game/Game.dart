import 'Board.dart';
import 'LevelGenerator.dart';
import '../Utils/Client.dart';
import 'Score.dart';


class Game  {

  Board board;
  Score score;
  LevelGenerator generator;
  int boardSize = 5;
  int maxRuleNumber = 3;
  int player1Moves,player2Moves;
  int player1Score, player2Score;
  Client client;

  Game( int iAmPlayer,Score s, String boardData){
  //  this.client = c;

    this.score = s;
    player1Moves = 0;
    player2Score = 0;
    player1Moves = 0;
    player2Moves = 0;
    if(boardData == null) {
      generator = new LevelGenerator(boardSize);
      board = new Board(generator.getData(), boardSize, iAmPlayer);
    }else{
      board = new Board(boardData, boardSize, iAmPlayer);
    }
  }

  Board getCurrentBoard(){
    return this.board;
  }



  void increaseMoves(int player){
    if(player == 1){
      player1Moves++;
    }else{
      player2Moves++;
    }
  }

  int getMoves(int player){
    if(player == 1){
      return player1Moves;
    }
    else{
      return player2Moves;
    }
  }



  bool playerCanStillMove(int pl){
    if(pl == getCurrentBoard().me){
      if(getCurrentBoard().getMyCurrentCoord().getRuleNumber() == 4 || getCurrentBoard().getMyCurrentCoord().getRuleNumber() == 3){
        for(int i=0; i<25;i++){
          if(!getCurrentBoard().getCoords(i).isBlocked()  && !getCurrentBoard().getMyCurrentCoord().assertEquals(getCurrentBoard().getCoords(i))){
            return true;
          }
        }
      }
    }else if(pl == getCurrentBoard().getEnemyPlayer()){
      if(getCurrentBoard().getEnemyCoords().getRuleNumber() == 4 || getCurrentBoard().getEnemyCoords().getRuleNumber() == 3){
        for(int i=0; i<25;i++){
          if(!getCurrentBoard().getCoords(i).isBlocked()  && !getCurrentBoard().getEnemyCoords().assertEquals(getCurrentBoard().getCoords(i))){
            return true;
          }
        }
      }
    }
    if(board.move("right", pl,true)){
      return true;
    }else if(board.move("left", pl,true)){
      return true;
    }
    else if(board.move("up", pl,true)){
      //pl == 1 ? oneDir_1 = true : oneDir_2 = true;
      return true;
    }
    else if(board.move("down", pl,true)){
     // pl == 1 ? oneDir_1 = true : oneDir_2 = true;
      return true;
    }

   return false;
  }

  Score getScore(){
    return this.score;
  }
  
}



