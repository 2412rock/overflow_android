import 'Coordinate.dart';
import 'dart:math';

class Board {
  int LENGTH, SIZE;
  String data;
  List<Coodrinate> boardCoords;
  bool gameOver;
  //Player player1,player2;
  int turn;
  int me;

  Board(String d, length, int iAm) {
    me = iAm;
    this.data = d;
    gameOver = false;
    //("Set placeHolder data: ${data}");
    this.LENGTH = length;
    SIZE = length * length;
    generateCoordinates(d);
  }

  int getLength() {
    return this.LENGTH;
  }

  int getSize() {
    return this.SIZE;
  }

  bool isNumeric(String str) {
    try {
      int.parse(str);
      return true;
    } catch (e) {
      return false;
    }
  }

  void generateCoordinates(data) {
      int i = 0;
      boardCoords = new List();
      for (int row = 0; row < 5; row++) {
        //MANUAL
        for (int col = 0; col < 5; col++) {
          Coodrinate instance = new Coodrinate(col, row);
          //(row.toString() + "," + col.toString() );
          while(true){
            if(isNumeric(data[i])){
              //("Found number -> " + data[i]);

              break;
            }else{
              i++;
            }
          }
          int initial = i;
          if (data[i + 1] == 'P') {
            String pl_number = data[i + 2];
            i++;
            i++;
            if (pl_number == "1") {
              //("Added PLAYER 1");
              instance.setPlayerOnThisPosition(1, true);
             // i++;
            } else if (pl_number == "2") {
              //("Added PLAYER 2");
              instance.setPlayerOnThisPosition(2, true);
             // i++;
            }

          }
          instance.setRuleNumber(int.parse(data[initial]));
          i++;
          boardCoords.add(instance);
          //("Added " + boardCoords.length.toString());
          if(col == 4 && row == 4){
            return;
          }
        }


    }
  }





  int getEnemyPlayer() {
    if (me == 1) {
      return 2;
    } else {
      return 1;
    }
  }

  Coodrinate getMyCurrentCoord() {
    for (int i = 0; i < boardCoords.length; i++) {
      if (boardCoords[i].playerIsOnThisPosition(me)) {
        return boardCoords[i];
      }
    }
  }

  Coodrinate getEnemyCoords() {
    for (int index = 0; index < boardCoords.length; index++) {
      if (boardCoords[index].playerIsOnThisPosition(getEnemyPlayer())) {
        return boardCoords[index];
      }
    }
  }


  void moveMeTo(int X, Y) {
    for (int index = 0; index < boardCoords.length; index++) {
      if (boardCoords[index].getX() == X && boardCoords[index].getY() == Y) {
        updatePosition(boardCoords[index], me);
      }
    }
  }

  void movePlayerTo(int player, Coodrinate c){

    for (int index = 0; index < boardCoords.length; index++) {
      if (boardCoords[index].getX() == c.getX() && boardCoords[index].getY() == c.getY()) {
        updatePosition(boardCoords[index], player);
      }
    }
  }


  void moveEnemyTo(int X, Y) {
    for (int index = 0; index < boardCoords.length; index++) {
      if (boardCoords[index].getX() == X && boardCoords[index].getY() == Y) {
        updatePosition(boardCoords[index], getEnemyPlayer());
      }
    }
  }

  void removeLastPlayerPosition(int player) {
    for (int i = 0; i < boardCoords.length; i++) {
      if (boardCoords[i].playerIsOnThisPosition(player)) {
        boardCoords[i].setPlayerOnThisPosition(player, false);
        boardCoords[i].startTransition();
      }
    }
  }

  int getEnemyForPlayer(int player) {
    if (player == 1) {
      return 2;
    } else {
      return 1;
    }
  }

  bool iWin(){
    int counter_me = 0;
    int counter_oponent = 0;
    for(int index=0;index<boardCoords.length;index++){
      if(boardCoords[index].isOwnedByPlayer(me)){
        counter_me++;
      }else if(boardCoords[index].isOwnedByPlayer(getEnemyPlayer())){
        counter_oponent++;
      }
    }
    return counter_me > counter_oponent;
  }

  bool itsATie(){
    int counter_me = 0;
    int counter_oponent = 0;
    for(int index=0;index<boardCoords.length;index++){
      if(boardCoords[index].isOwnedByPlayer(me)){
        counter_me++;
      }else if(boardCoords[index].isOwnedByPlayer(getEnemyPlayer())){
        counter_oponent++;
      }
    }
    return counter_me == counter_oponent;
  }

  Coodrinate getCoordForPlayer(int player){
    for(int i=0; i<boardCoords.length; i++){
      if(boardCoords[i].playerIsOnThisPosition(player)){
        return boardCoords[i];
      }
    }
  }

  bool updatePosition(Coodrinate c, int player, [bool simulation = false]) {

    Coodrinate current;
    if (player == me) {
      current = getMyCurrentCoord();
    } else {
      current = getEnemyCoords();
    }

    for (int coordIndex = 0; coordIndex < boardCoords.length; coordIndex++) {
      if (boardCoords[coordIndex].assertEquals(c) && !boardCoords[coordIndex].playerIsOnThisPosition(getEnemyForPlayer(player)) && !boardCoords[coordIndex].isOwnedByPlayer(1) && !boardCoords[coordIndex].isOwnedByPlayer(2)) {
        if(!simulation) {
          current.movedFromHere = true;//will be updated back to false after animation occurs
          removeLastPlayerPosition(player);
          boardCoords[coordIndex].setPlayerOnThisPosition(player, true);

          boardCoords[coordIndex].movedHere = true;
        }
        return true;
      }
    }
    //POSITION IS INVALID
   // updatePosition(current, player);
    return false;
  }

  bool move(var d,int pl,bool simulation ) {
    Coodrinate newCoord;

    if(pl == this.me && !simulation &&getMyCurrentCoord().getRuleNumber() == 4 ){

        List<int> crds = new List();
        for(int i=0; i<25 ; i++){
          if(!getCoords(i).isBlocked() && getCoords(i).getRuleNumber() != 4){
            crds.add(i);
          }
        }

       // return true;
        if(crds.length > 0) {
          int randomIndex = new Random().nextInt(crds.length);
          Coodrinate crt = getMyCurrentCoord();
          crt.movedFromHere = true;
          moveMeTo(
              getCoords(randomIndex).getX(), getCoords(randomIndex).getY());
          getMyCurrentCoord().startTransition();
          return true;
        }


    }
    switch (d) {
      case "down":
        if(pl == this.me) {
          newCoord = getNewCoordinateForMove("down", getMyCurrentCoord());
         // me = true;

        }else{
          newCoord = getNewCoordinateForMove("down", getEnemyCoords());
         // me =false;
        }
        ;
        break;
      case "up":
        if(pl == this.me) {
          newCoord = getNewCoordinateForMove("up", getMyCurrentCoord());
          //me =true;
        }else{
          newCoord = getNewCoordinateForMove("up", getEnemyCoords());
         // me = false;
        }
        break;
      case "right":
        if(pl == this.me) {
          newCoord = getNewCoordinateForMove("right", getMyCurrentCoord());
          //me = true;
        }else{
          newCoord = getNewCoordinateForMove("right", getEnemyCoords());
         // me = false;
        }
        break;
      case "left":
        if(pl == this.me) {
          newCoord = getNewCoordinateForMove("left", getMyCurrentCoord());
         // me = true;
        }else{
          newCoord = getNewCoordinateForMove("left", getEnemyCoords());
        //  me = false;
        }
        break;
      default:
        //("invalid");
        return false;

    }
      bool updated;
       updated = updatePosition(newCoord,pl,simulation);
    return updated;
  }

  Coodrinate getCoords(int index) {
    return boardCoords[index];
  }

  bool cellIsReachableBySwipe(int player,int index,var dir){

    Coodrinate c = player == me ? getMyCurrentCoord() : getEnemyCoords();
    if(c.getRuleNumber() == 3 || c.getRuleNumber() == 4 && !getCoords(index).isBlocked() && !getCoords(index).assertEquals(c)){
      return true;
    }

       c = player == 1 ? getMyCurrentCoord() : getEnemyCoords();

      if(move(dir, player == 1 ? 1 : 2, true)){
        Coodrinate g = getNewCoordinateForMove(dir, c);
        if(g.assertEquals(boardCoords[index])){
          return true;
        }
      }
      return false;
  }

  bool cellIsReachableByPlayer(int player, int index){
    Coodrinate c = player == me ? getMyCurrentCoord() : getEnemyCoords();
    if(c.getRuleNumber() == 3 || c.getRuleNumber() == 4 && !getCoords(index).isBlocked() && !getCoords(index).assertEquals(c)){
      return true;
    }
       c = (player == 1 && me == 1 )|| (player == 2 && me == 2)? getMyCurrentCoord() : getEnemyCoords();
      if(move("up", player == 1 ? 1 : 2, true)){
        Coodrinate g = getNewCoordinateForMove("up", c);
        if(g.assertEquals(boardCoords[index])){
          return true;
        }
      }
      if(move("down", player == 1 ? 1 : 2, true)){
        Coodrinate g = getNewCoordinateForMove("down", c);
        if(g.assertEquals(boardCoords[index])){
          return true;
        }
      }
      if(move("left", player == 1 ? 1 : 2, true)){
        Coodrinate g = getNewCoordinateForMove("left", c);
        if(g.assertEquals(boardCoords[index])){
          return true;
        }
      }
      if(move("right", player == 1 ? 1 : 2, true)){
        Coodrinate g = getNewCoordinateForMove("right", c);
        if(g.assertEquals(boardCoords[index])){
          return true;
        }
      }
      return false;

    }


  Coodrinate getNewCoordinateForMove(
      String direction, Coodrinate currentCoord) {
    int boundsCounter = coordCounter(direction, currentCoord);
    switch (direction) {
      case "right":
        if (boundsCounter >= getLength()) {
          return getWrapAroundCoordinate("right", currentCoord, boundsCounter);
        } else {
          return getSimpleCoordinate("right", currentCoord);
        }
        break;
      case "left":
        if (boundsCounter < 0) {
          return getWrapAroundCoordinate("left", currentCoord, boundsCounter);
        } else {
          return getSimpleCoordinate("left", currentCoord);
        }
        break;
      case "up":
        if (boundsCounter < 0) {
          return getWrapAroundCoordinate("up", currentCoord, boundsCounter);
        } else {
          return getSimpleCoordinate("up", currentCoord);
        }
        break;
      case "down":
        if (boundsCounter >= getLength()) {
          return getWrapAroundCoordinate("down", currentCoord, boundsCounter);
        } else {
          return getSimpleCoordinate("down", currentCoord);
        }
        break;
      default:
        //("invalid move");
        return null;
    }
  }



  int coordCounter(String direction, Coodrinate crt) {
    int xAxis = crt.getX(), yAxis = crt.getY();
    for (int count = 0; count < crt.getRuleNumber(); count++) {
      if (direction == "right" || direction == "down") {
        xAxis++;
        yAxis++;
      } else if (direction == "left" || direction == "up") {
        xAxis--;
        yAxis--;
      }
    }
    if (direction == "down" || direction == "up") {
      return yAxis;
    } else if (direction == "right" || direction == "left") {
      return xAxis;
    }
  }

  Coodrinate getWrapAroundCoordinate(String direction, Coodrinate crt, int b) {
    if (direction == "right") {
      int newX = b - getLength();
      return new Coodrinate(newX, crt.getY());
    } else if (direction == "down") {
      int newY = b - getLength();
      return new Coodrinate(crt.getX(), newY);
    } else if (direction == "left") {
      int newX = getLength() + b;
      return new Coodrinate(newX, crt.getY());
    } else if (direction == "up") {
      int newY = getLength() + b;
      return new Coodrinate(crt.getX(), newY);
    }
  }

  Coodrinate getSimpleCoordinate(String direction, Coodrinate crt) {
    if (direction == "left") {
      return new Coodrinate(crt.getX() - crt.getRuleNumber(), crt.getY());
    } else if (direction == "right") {
      return new Coodrinate(crt.getX() + crt.getRuleNumber(), crt.getY());
    } else if (direction == "up") {
      return new Coodrinate(crt.getX(), crt.getY() - crt.getRuleNumber());
    } else if (direction == "down") {
      return new Coodrinate(crt.getX(), crt.getY() + crt.getRuleNumber());
    }
  }
}
