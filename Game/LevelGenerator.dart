import 'dart:math';

class LevelGenerator{

  int length;
  final int maxValue = 4;
  int player1InitialPosition;
  int player2InitialPosition;
  int size;
  String data;
  List<int> ruleNumbersList;
 
  LevelGenerator(int len){
    //this.maxValue = maxValue;
    this.length = len;
    size = length*length;
    ruleNumbersList = new List(size);
    generateRuleNumberList();
    generatePlayersPositin();
    putRuleNumbersInString();
  }

  int add2sToList(){
    int nrOf2s = getNumberOf2s();
    for(int i=0; i<nrOf2s; i++){
      ruleNumbersList[i] = 2;
    }
    return nrOf2s;
  }

  void generateRuleNumberList(){
    int nrOf2s = add2sToList();
    int nrOf1s = 21 - nrOf2s;
    //start at the index where 2 was last added
    int nrOf1sAdded = 0;
    for(int i=nrOf2s-1; i<size;i++){
      if(nrOf1sAdded == nrOf1s){
        break;
      }else{
        ruleNumbersList[i] = 1;
      }
    }
    ruleNumbersList[23] = 3;
    ruleNumbersList[24] = 4;
    //21 - numberOf2s + 3 + 4
    while(true){
      ruleNumbersList = shuffle(ruleNumbersList);
      if(ruleNumbersList[0] != 3 && ruleNumbersList[0] != 4  &&ruleNumbersList[0] != 2 && ruleNumbersList[24] != 3 && ruleNumbersList[24] != 4  && ruleNumbersList[24] != 2){
        break;
      }
    }

  }

  int getNumberOf2s(){
    // MODIFY THIS IF NECESSARY
    int min = 3;
    int max = 6;
    Random rnd = new Random();
    int res = rnd.nextInt(max);
    if(res < 4){
      res = res +  min;
    }
    //print("$res is in the range of $min and $max");
    return res;
  }

  List shuffle(List items) {
    var random = new Random();

    // Go through all elements.
    for (var i = items.length - 1; i > 0; i--) {

      // Pick a pseudorandom number according to the list length
      var n = random.nextInt(i + 1);

      var temp = items[i];
      items[i] = items[n];
      items[n] = temp;
    }

    return items;
  }



  int getRuleNumber(int index){
    return ruleNumbersList[index];
  }

  int getSize(){
    return this.size;
  }

  int getLength(){
    return this.length;
  }

  void generatePlayersPositin(){
    player1InitialPosition = 0;
    player2InitialPosition = size-1;//idk why

    
  }

  void putRuleNumbersInString(){
    String result = " ";
    for(int i=0; i<size;i++){
      if(i == player1InitialPosition){
        result = result + " " + ruleNumbersList[i].toString() + "P1";
      }
      else if(i == player2InitialPosition){
        result = result + " " + ruleNumbersList[i].toString() + "P2";
      }
      else{
        result = result + " " + ruleNumbersList[i].toString();
      }
    }
   // print(result);
    this.data = result;    
  }

  String getData(){
    return this.data;
  }
}