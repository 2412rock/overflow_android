

class Score{

  int player1Score,player2Score;

  Score(){
    player1Score = 0;
    player2Score = 0;
  }

  void increasePlayerScore(int player){
    if(player == 1){
      player1Score++;
    }
    else{
      player2Score++;
    }

    if(player1Score > 99 || player2Score > 99){
      player1Score = 0;
      player2Score = 0;
    }
  }

  String getPLayerScore(int player){
    String r;
    player == 1 ? r = player1Score.toString() : r = player2Score.toString();
    return r;
  }

}