
class Coodrinate{
  int _x, _y;
  int ruleNumber;
  bool playerOnthisPosition = false, mt = false;
  bool movedFromHere,movedHere;
  bool _t;
  bool blocked;
  int ownedByPlayer;

  Coodrinate(this._x, this._y){
    movedFromHere = false;
    _t = false;
    blocked = false;
    movedHere = false;
    ownedByPlayer = 0;
  }
  
  void setRuleNumber(int nr){
    ruleNumber = nr;
  }

  int getRuleNumber(){
    return ruleNumber;
  }

  bool isBlocked(){
    return blocked;
  }




  bool movable_to(){
    return this.mt;

  }


  String getRuleNumberAsString(){
    return ruleNumber.toString();
  }

  int getX(){
    return _x;
  }

  int getY(){
    return _y;
  }

  void startTransition(){
    _t = true;
  }

  void endTransition(){
    _t = false;
  }

  bool tValue(){
    return _t;
  }

  bool playerIsOnThisPosition(int playerNumber){
    if(isOwnedByPlayer(playerNumber) && playerOnthisPosition ){

      return true;
    }
    return false;
  }

  void setPlayerOnThisPosition(int playerNumber,bool v){
    blocked = true;
       playerOnthisPosition = v;
      ownedByPlayer = playerNumber;
       startTransition();
  }

  bool wasOwnedByPlayer(int p){
    return ownedByPlayer == p;
  }

  bool isOwnedByPlayer(int playerNumber){
    return ownedByPlayer == playerNumber;
  }

  bool isTeritoryOf(int playerNumber){
    return !playerOnthisPosition && isOwnedByPlayer(playerNumber);
  }

  bool assertEquals(Coodrinate c){
    return _x == c.getX() && _y == c.getY();
  }
}